#version 300 es
precision mediump float;

struct DirLight {
    vec3 position;
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
in vec4 FragPosLightSpace;
in vec2 TexCoords;
in vec3 VecNormal;  
in vec3 FragPos;  
out vec4 FragColor;
#define NR_POINT_LIGHTS 8
uniform DirLight dirLight;
uniform PointLight pointLights[NR_POINT_LIGHTS];

uniform vec3 viewPos;

uniform mediump sampler2D diffuse;
uniform mediump sampler2D shadowMap;

float ShadowCalculation(vec3 lightDir, vec3 normal)
{
	//vec3 normal = normalize(FragNormal);

	 // perform perspective divide
    vec3 projCoords = FragPosLightSpace.xyz / FragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
	if(projCoords.z > 1.0)
         return 0.0;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
	float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);

  //  float closestDepth = texture(shadowMap, vec3(projCoords.xy, bias));   

    // Get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // Check whether current frag pos is in shadow
	float shadow = 0.0;
	vec2 texelSize = 1.0 / vec2(textureSize(shadowMap, 0));
	for(int x = -1; x <= 1; ++x)
	{
		for(int y = -1; y <= 1; ++y)
		{
			float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
			shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;        
		}    
	}
	shadow /= 9.0;

    return shadow;
}  


const float shininess = 32.f;
// Calculates the color when using a directional light.
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(dirLight.position - FragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
    // Combine results
    vec3 ambient = light.ambient * vec3(texture(diffuse, TexCoords));
    vec3 diffuseComponent  = light.diffuse  * diff;
    vec3 specular = light.specular * spec;
    float shadow =  ShadowCalculation(lightDir, normal);

     return (ambient + (1.0-shadow) * (diffuseComponent + specular) * texture(diffuse, TexCoords).rgb);
}

// Calculates the color when using a point light.
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
    // Attenuation
    float distance = length(light.position - fragPos);
	float constTerm = max(light.constant, 2.0);
	
    float attenuation = 1.0f / (constTerm + light.linear * distance + light.quadratic * (distance * distance));    
    // Combine results
    vec3 ambient = light.ambient * vec3(texture(diffuse, TexCoords));
    vec3 diffuse  = light.diffuse  * diff * vec3(texture(diffuse, TexCoords));
    vec3 specular = light.specular * spec;
    
	ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);
}


void main() 
{
	vec3 norm = normalize(VecNormal);
    vec3 viewDir = normalize(viewPos - FragPos);
	
    // Phase 1: Directional lighting
    vec3 result = CalcDirLight(dirLight, norm, viewDir);
    // Phase 2: Point lights
    for(int i = 0; i < NR_POINT_LIGHTS; i++)
        result += CalcPointLight(pointLights[i], norm, FragPos, viewDir);    
    vec3 projCoords = FragPosLightSpace.xyz / FragPosLightSpace.w;
    vec3 lightDir = normalize(dirLight.position - FragPos);

//	float bias = max(0.05 * (1.0 - dot(norm, lightDir)), 0.005);

   // float closestDepth = texture(shadowMap, vec3(projCoords.xy, bias)); 
	float depth = texture(shadowMap, vec2(0.5f,0.5f)).r;
	FragColor = vec4(result,1);
	//FragColor = vec4(vec3(depth,0,0), 1.0f);
}
