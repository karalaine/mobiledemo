#version 300 es
precision mediump float;
struct PointLight {
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
struct DirLight {
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};
#define NR_POINT_LIGHTS 8

layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec3 Normal;
layout(location = 2) in vec2 TexCoord;
layout(location = 3) in vec3 Tangent;
layout(location = 4) in vec3 Bitangent;


out vec4 FragPosLightSpace;
out vec2 TexCoords;
out vec3 FragNormal;
out vec3 TangentDirLightPos;
out vec3 TangentPointLightPos[NR_POINT_LIGHTS];
out vec3 TangentViewPos;
out vec3 TangentFragPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform mat3 normalMatrix;
uniform vec3 lightPos;
uniform vec3 viewPos;
uniform mat4 lightSpaceMatrix;


uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform DirLight dirLight;


void main() {
	float y = VertexPosition.y;
	gl_Position = proj * view * model * vec4(VertexPosition.x, y, VertexPosition.z, 1.0f);
	vec3 FragPos = vec3(model * vec4(VertexPosition, 1.0));

	TexCoords = TexCoord;
	FragNormal = transpose(inverse(mat3(model))) * Normal;

	vec3 T = normalize(normalMatrix * Tangent);
	vec3 B = normalize(normalMatrix * Bitangent);
	vec3 N = normalize(normalMatrix * Normal);

	mat3 TBN = transpose(mat3(T, B, N));

	TangentDirLightPos = TBN * dirLight.position;
	for(int i = 0; i < NR_POINT_LIGHTS; i++)
		TangentPointLightPos[i] = TBN * pointLights[i].position;
		
	TangentViewPos = TBN * viewPos;
	TangentFragPos = TBN * FragPos;
	FragPosLightSpace = lightSpaceMatrix * vec4(FragPos, 1.0);
}