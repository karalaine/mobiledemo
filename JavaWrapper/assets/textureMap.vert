#version 300 es
precision mediump float;

in vec3 VertexPosition;
in vec3 Normal;
in vec2 TexCoord; 
in vec3 Tangent;
in vec3 Bitangent;

out vec3 FragPos;
out vec2 TexCoords;
out vec3 TangentLightPos;
out vec3 TangentViewPos;
out vec3 TangentFragPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform vec3 lightPos;
uniform vec3 viewPos;

void main() {
	gl_Position = proj * view * model * vec4(VertexPosition, 1.0f);
	FragPos = vec3(model * vec4(VertexPosition, 1.0));
	TexCoords = TexCoord;

	vec3 T = normalize(vec3(model * vec4(tangent, 0.0)));
	vec3 N = normalize(vec3(model * vec4(normal, 0.0)));
	// re-orthogonalize T with respect to N
	T = normalize(T - dot(T, N) * N);
	// then retrieve perpendicular vector B with the cross product of T and N
	vec3 B = cross(T, N);

	mat3 TBN = mat3(T, B, N)  
	TangentLightPos = TBN * lightPos;
	TangentViewPos = TBN * viewPos;
	TangentFragPos = TBN * FragPos;
}