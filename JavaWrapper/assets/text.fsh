#version 300 es
precision highp float;
in vec2 TexCoords;
out vec4 color;

uniform sampler2D textAtlas;

void main()
{    
	vec4 c = texture(textAtlas, TexCoords);
	if(c.r < 0.01)
		discard;
	color = vec4(c.r, c.r, c.r, c.r);
}