#version 300 es
precision mediump float;
in vec3 VertexPosition;
in vec3 Normal;
in vec2 TexCoord;
out vec3 VecNormal;
out vec3 FragPos;
out vec2 TexCoords;
out vec4 FragPosLightSpace;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform mat3 normalMatrix;
uniform mat4 lightSpaceMatrix;

void main() {
	gl_Position = proj * view * model * vec4(VertexPosition, 1.0f);
	FragPos = vec3(model * vec4(VertexPosition, 1.0f));
	VecNormal = normalMatrix * Normal;
	TexCoords = vec2(TexCoord.x, 1.0f - TexCoord.y);
	FragPosLightSpace = lightSpaceMatrix * vec4(FragPos, 1.0);
}