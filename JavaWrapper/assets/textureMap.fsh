#version 300 es
precision mediump float;
struct PointLight {
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
struct DirLight {
    vec3 position;
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

#define NR_POINT_LIGHTS 8
in vec4 FragPosLightSpace;
in vec3 FragNormal;
in vec2 TexCoords;
in vec3 TangentDirLightPos;
in vec3 TangentPointLightPos[NR_POINT_LIGHTS];
in vec3 TangentViewPos;
in vec3 TangentFragPos;

out vec4 FragColor;

uniform lowp sampler2DArray TextureArray;
uniform lowp sampler2D shadowMap;
uniform int texCount;
uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform DirLight dirLight;
uniform mat4 lightSpaceMatrix;
const float height_scale = 0.1f;

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir)
{ 
    // number of depth layers
    const float minLayers = 10.0f;
    const float maxLayers = 20.0f;
    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));  
    // calculate the size of each layer
    float layerDepth = 1.0 / numLayers;
    // depth of current layer
    float currentLayerDepth = 0.0;
    // the amount to shift the texture coordinates per layer (from vector P)
    vec2 P = viewDir.xy / viewDir.z * height_scale; 
    vec2 deltaTexCoords = P / numLayers;
  
    // get initial values
    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = texture(TextureArray, vec3(currentTexCoords, 3)).r;

    while(currentLayerDepth < currentDepthMapValue)
    {
        // shift texture coordinates along direction of P
        currentTexCoords -= deltaTexCoords;
        // get depthmap value at current texture coordinates
        currentDepthMapValue = texture(TextureArray, vec3(currentTexCoords, 3)).r;
        // get depth of next layer
        currentLayerDepth += layerDepth;  
    }
    
    // -- parallax occlusion mapping interpolation from here on
    // get texture coordinates before collision (reverse operations)
    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

    // get depth after and before collision for linear interpolation
    float afterDepth  = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = texture(TextureArray, vec3(prevTexCoords, 3)).r - currentLayerDepth + layerDepth;
 
    // interpolation of texture coordinates
    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;
}

float ShadowCalculation(vec3 lightDir, vec3 normal)
{
    // perform perspective divide
    vec3 projCoords = FragPosLightSpace.xyz / FragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
	if(projCoords.z > 1.0)
		return 0.0;
	float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
    float currentDepth = projCoords.z;
    // Calculate bias (based on depth map resolution and slope)
   
    // Check whether current frag pos is in shadow
    // float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;
    // PCF
    float shadow = 0.0;
    vec2 texelSize = 1.0 / vec2(textureSize(shadowMap, 0));
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
            shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;        
        }    
    }
    shadow /= 9.0;

    return shadow;
}  

vec3 CalcDirLight(vec2 texCoords, vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(TangentDirLightPos - TangentFragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
	vec3 halfwayDir = normalize(lightDir + viewDir);  
    float spec = pow(max(dot(normal, halfwayDir), 0.0), 16.0);
    // Combine results
	vec3 diffuseMap = texture(TextureArray, vec3(texCoords, 0)).rgb;
	float shadow =  ShadowCalculation(lightDir, normal);
    vec3 ambient = dirLight.ambient * diffuseMap;
    vec3 diffuse  = dirLight.diffuse  * diff * diffuseMap;
    vec3 materialSpecular = vec3(0.5f);
	if(texCount > 2)
	{
		materialSpecular = texture(TextureArray, vec3(texCoords, 2)).rgb;
	}
    vec3 specular = dirLight.specular * spec * materialSpecular;

    return (ambient + (1.0-shadow) * (diffuse + specular));
}

vec3 CalcPointLight(vec2 texCoords, PointLight light, vec3 lightPos, vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(lightPos - TangentFragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32.0);
    // Attenuation
    float distance = length(lightPos - TangentFragPos);
	float constTerm = max(light.constant, 2.0);
	
    float attenuation = 1.0f / (constTerm + light.linear * distance + light.quadratic * (distance * distance));    
    // Combine results
    vec3 diffuseMap = texture(TextureArray, vec3(texCoords, 0)).rgb;
    vec3 ambient = light.ambient * diffuseMap;
    vec3 diffuse = light.diffuse  * diff * diffuseMap;

    vec3 materialSpecular = vec3(0.5f);
	if(texCount > 2)
	{
		materialSpecular = vec3(texture(TextureArray, vec3(texCoords, 2)));
	}

    vec3 specular = light.specular * spec * materialSpecular;
	ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);
}

void main() 
{
	vec3 viewDir = normalize(TangentViewPos - TangentFragPos);
 	
	vec2 texCoords = TexCoords;
	if(texCount > 3)
		texCoords = ParallaxMapping(TexCoords, viewDir);

	vec3 normal = texture(TextureArray, vec3(texCoords, 1)).rgb;
    // Transform normal vector to range [-1,1]
    normal = normalize(normal * 2.0 - 1.0);  // this normal is in tangent space
 	
	vec3 result = CalcDirLight(texCoords, normal, viewDir);
	
	// Phase 2: Point lights
	for(int i = 0; i < NR_POINT_LIGHTS; i++)
       result += CalcPointLight(texCoords, pointLights[i], TangentPointLightPos[i], normal, viewDir);    

	FragColor = vec4(result, 1.0f);
}
