#version 300 es
precision mediump float;
in vec3  VertexPosition;
in vec3  Normal;
in vec2  TexCoord;
out vec3 VecNormal;
out vec3 FragPos;
out vec2 TexCoords;
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main() {
	gl_Position = proj * view * model * vec4(VertexPosition, 1.0f);
	FragPos = vec3(model * vec4(VertexPosition, 1.0f));
	VecNormal = mat3(transpose(inverse(model))) * Normal;
	TexCoords = vec2(TexCoord.x, 1.0f - TexCoord.y);
}
