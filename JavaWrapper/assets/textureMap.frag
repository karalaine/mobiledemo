#version 450
precision mediump float;
in VS_OUT {
    vec3 FragPos;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} fs_in;

out vec4 FragColor;

uniform sampler2DArray TextureArray;
uniform int texCount;

void main() 
{
	
	vec3 lightColor = vec3(1.0f);
	vec3 normal = vec3(texture(TextureArray, vec3(fs_in.TexCoords, 1)));
    // Transform normal vector to range [-1,1]
    normal = normalize(normal * 2.0 - 1.0);  // this normal is in tangent space

	vec3 lightDir = normalize(fs_in.TangentLightPos - fs_in.TangentFragPos);

	float ambientStrength = 0.1f;
    vec3 ambient = ambientStrength * lightColor;
		
	float diff = max(dot(normal, lightDir), 0.0f);
	vec3 diffuse = diff * lightColor;
	
	vec3 specularStrength = vec3(0.5f, 0.5f, 0.4f);
    vec3 viewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);
    vec3 reflectDir = reflect(-lightDir, normal);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0f), 32.0f);
    vec3 materialSpecular = vec3(0);
	if(texCount >= 2)
	{
		materialSpecular = vec3(texture(TextureArray, vec3(fs_in.TexCoords, 2)));
	}
	else
	{
		materialSpecular = vec3(0.5f);
	}
	vec3 specular = specularStrength * spec * materialSpecular;  

	vec3 result = (ambient + diffuse + specular);
	vec4 color = texture(TextureArray, vec3(fs_in.TexCoords, 0));
	FragColor = color * vec4(result, 1.0f);
}
