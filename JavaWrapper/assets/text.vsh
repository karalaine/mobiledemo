#version 300 es
precision mediump float;
layout (location = 0) in vec2 VertexPosition;
layout (location = 1) in vec2 TexCoord;
out vec2 TexCoords;
uniform mat4 projection;

void main() {
	gl_Position = projection * vec4(VertexPosition, 0.0f, 1.0f);
	TexCoords = TexCoord;
}