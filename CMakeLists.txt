cmake_minimum_required (VERSION 3.16)
project (MobileDemo)
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/CMake)
set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED True)
file(GLOB COMMON_SRC "src/*.c" "src/*.cpp" "include/*.h")
if(ANDROID)
	add_library(MobileDemo SHARED ${COMMON_SRC})	
else()
	add_executable(MobileDemo ${COMMON_SRC})
endif()



if(WIN32)
	set(DEMO_PLATFORM win32)
	target_include_directories(MobileDemo PUBLIC "src/desktop/include/")
	add_compile_definitions(NOMINMAX USE_WGL WIN32_LEAN_AND_MEAN)
	find_package(OpenGL REQUIRED)
elseif(UNIX)

	#find_package(GLESv2 REQUIRED)
	if(NOT DEFINED ANDROID)		
		if(APPLE)
			target_include_directories(MobileDemo PUBLIC
				"src/desktop/include/"
				"src/GLFW/include")
			set(CMAKE_OSX_ARCHITECTURES "x86_64" CACHE STRING "")
			set(EGL_INCLUDE_DIR "src/desktop/include")
			set(EGL_LIBRARY "${CMAKE_SOURCE_DIR}/external/angle/libEGL.dylib")

			find_package(GLFW REQUIRED)
			set(DEMO_PLATFORM macos)
		else()
			set(DEMO_PLATFORM unix)
			find_package(X11 REQUIRED)
			find_package(Xi REQUIRED)
		endif()
		target_include_directories(MobileDemo PUBLIC "src/posix/include/")
		find_package(UUID REQUIRED uuid)
	else()
		set(DEMO_PLATFORM android)
	endif()
	message(STATUS "Looking for EGL: ${EGL_INCLUDE_DIR} ${EGL_LIBRARY}")
	find_package(EGL REQUIRED)
	add_definitions(
		${EGL_DEFINITIONS}
		${GLESv2_DEFINITIONS}
	)

endif()


target_include_directories(MobileDemo PUBLIC
	include
	src/${DEMO_PLATFORM}/include 
	external/rocket/lib
	)

#Sources
file(GLOB PLATFORM_SRC 
	"src/${DEMO_PLATFORM}/*.c" 
	"src/${DEMO_PLATFORM}/*.cpp"
 	"external/rocket/lib/*.cpp")

if(WIN32 OR APPLE)
	file(GLOB DESKTOP_SRC
		"src/bass/*.cpp"
		"src/std/*.cpp"
		"src/desktop/*.c")
	list(APPEND PLATFORM_SRC 
		${DESKTOP_SRC})
	target_include_directories(MobileDemo PUBLIC external/bass)
endif()

if(APPLE)
	target_sources(MobileDemo
		PRIVATE
		"src/macos/display_helper.mm"
		"src/GLFW/include/GLFWWindow.h"
		"src/GLFW/GLFWWindow.cpp")
endif()

if(UNIX)
	file(GLOB POSIX_SRC
		"src/posix/*.cpp")
	list(APPEND PLATFORM_SRC 
		${POSIX_SRC})
endif()

message("Plaform sources:  ${PLATFORM_SRC}")
set(SOURCES ${PLATFORM_SRC})

find_package(GLM REQUIRED)
find_package(assimp REQUIRED)
target_include_directories(MobileDemo PUBLIC
	${ASSIMP_INCLUDE_DIR} 
	${GLM_INCLUDE_DIRS}
)
set(MobileDemoLibs ${ASSIMP_LIBRARIES})
if(UNIX)
	target_include_directories(MobileDemo PUBLIC
		${UUID_INCLUDE_DIRS}
		${EGL_INCLUDE_DIR}
	)
	target_compile_options(MobileDemo PRIVATE -Wall -Wextra -pedantic)
	if(NOT DEFINED ANDROID)
		if(APPLE)
			list(APPEND MobileDemoLibs 
				${CMAKE_SOURCE_DIR}/external/bass/x64/libbass.dylib
				${GLFW_LIBRARIES})
			target_compile_definitions(MobileDemo PRIVATE KTX_OPENGL KTX_USE_GETPROC USE_GLAD)
		else()
			list(APPEND MobileDemoLibs
				"${X11_LIBRARIES}"
				"${XI_LIBRARIES}"
				${CMAKE_SOURCE_DIR}/external/bass/x64/libbass.so)
		endif()		
		list(APPEND MobileDemoLibs
			${GLESv2_LIBRARIES}
			${EGL_LIBRARY})
	else()
		find_library(LOGLIB log)
		find_library(ANDROID_LIBRARIES android)
		find_library(GLESv3_LIBRARIES GLESv3)
		find_library(ANDROID_AUDIO_LIBRARIES aaudio)
		find_library(OpenSLES_LIBRARIES OpenSLES)

		list(APPEND MobileDemoLibs ${LOGLIB}
				${ANDROID_LIBRARIES}
				${ANDROID_AUDIO_LIBRARIES}
				${OpenSLES_LIBRARIES}
				"${UUID_LIBRARIES}"
				${GLESv2_LIBRARIES}
				${GLESv3_LIBRARIES}
				${EGL_LIBRARIES})
	endif()
elseif(WIN32)
	list(APPEND MobileDemoLibs 
		${CMAKE_SOURCE_DIR}/external/bass/x64/bass.lib
		${OPENGL_gl_LIBRARY}
	)
	if( CMAKE_COMPILER_IS_MINGW )
		target_compile_options(MobileDemo PRIVATE -Wall -Wextra -pedantic)
		target_link_libraries(MobileDemo ws2_32 wsock32 assimp zlibstatic IrrXML -static-libgcc -static-libstdc++)
		set(CMAKE_EXE_LINKER_FLAGS "-static")
	endif()
	target_compile_definitions(MobileDemo PRIVATE KTX_OPENGL USE_GLAD)
	if(MSVC14)
		ASSIMP_COPY_BINARIES(${PROJECT_BINARY_DIR})
		add_dependencies(MobileDemo AssimpCopyBinaries)
	endif()
	set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT MobileDemo)
endif()
target_sources(MobileDemo PRIVATE ${SOURCES})

target_link_libraries(MobileDemo ${MobileDemoLibs})
