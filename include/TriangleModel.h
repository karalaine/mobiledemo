#pragma once
#include "Node.h"
#ifdef USE_GLAD
#include <glad/glad.h>
#else
#include <GLES3/gl3.h>
#endif

class TriangleModel : public Node
{
  public:
	TriangleModel();
	virtual ~TriangleModel();
	virtual void Render();
	virtual void InitNode();

  private:
	GLuint positionAttribHandle;
	GLuint colorAttribHandle;
	GLuint radianAngle;
	GLuint vbos[3];
	GLuint vao;
	float degree = 0;
	float radian;
	enum vboType
	{
		VERTEX = 0,
		COLOR,
		INDEX
	};
};
