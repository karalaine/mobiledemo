#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <string>
#include <glm/gtc/type_ptr.hpp>
struct aiMesh;

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
	glm::vec3 Tangent;
	glm::vec3 Bitangent;
};

class Mesh
{
public:
	Mesh() = default;
	Mesh(std::vector<Vertex>&& m_vertices, std::vector<unsigned int>&& m_indices, const std::string& materialId);
	Mesh(const aiMesh*, const std::string& materialId);

	const void * GetVertexData() {
		return m_vertices.data();
	}

	size_t GetVertexCount()
	{
		return m_vertices.size();
	}

	std::vector<Vertex>& GetVertices()
	{
		return m_vertices;
	}

	unsigned int * GetIndexData() {
		return &m_indices[0];
	}
	size_t GetIndexCount()
	{
		return m_indices.size();
	}

	std::vector<unsigned int>& GetIndices()
	{
		return m_indices;
	}

	unsigned int GetTriangleCount()
	{
		return m_triangleCount;
	}

	std::string& GetMaterialGuid()
	{
		return m_materialGuid;
	};
private:
	std::vector<Vertex> m_vertices;
	std::vector<unsigned int> m_indices;
	std::string m_materialGuid;
	unsigned int m_triangleCount;
};