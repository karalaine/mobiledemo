#pragma once
#include "Node.h"

class LoaderBar :
	public Node
{
public:
	LoaderBar();
	virtual ~LoaderBar();

	// Inherited via Node
	virtual void InitNode() override;
	virtual void RenderOpaque() override;
private:
	GLuint vao, vbo;
	GLint positionAttribHandle;
};

