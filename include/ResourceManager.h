#pragma once
#include <unordered_map>
#include <string>
#include "AssetManager.h"
#include "Shader.h"

template<class T>
class ResourceManager
{
public:
	ResourceManager() = default;
	auto Add(const std::string& guid, T&& resource)
	{
		return m_resources.emplace(guid, std::move(resource));
	}

	std::unordered_map<std::string, T>& All()
	{
		return m_resources;
	}

	T * Get(const std::string & guid)
	{
		auto it = m_resources.find(guid);
		T * resource = nullptr;
		if (it != m_resources.end())
		{
			resource = &(it->second);
		}
		return resource;
	}

	
private:
	std::unordered_map<std::string, T> m_resources;
};


