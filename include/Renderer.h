#pragma once
#include <EGL/egl.h>

#include <vector>
#include <memory>


class Scene;
class RenderPass;

class Renderer
{
public:
    Renderer();
    virtual ~Renderer();

    int InitDisplay(EGLNativeWindowType window);
    void TermDisplay();

    void InitializeScenes();  
    // Initialize Engine 
    void Resize( int w, int h );// resize screen 
    void Render();            
    // Render the Scenes 
    void AddScene(std::unique_ptr<Scene>&& scene);
    // Add new scene   
    bool RemoveScene( Scene* scene); // Remove the scene
	void AddRenderPass(std::unique_ptr<RenderPass>&& renderPass);
	EGLint GetWidth();
	EGLint GetHeight();
	std::vector <std::unique_ptr<Scene>>& GetScenes()
	{
		return m_scenes;
	}

private:

    EGLDisplay m_display;
    std::vector <std::unique_ptr<Scene>> m_scenes;
	std::vector <std::unique_ptr<RenderPass>> m_renderPasses;
	EGLint m_width, m_height;
    EGLContext m_context;
    EGLSurface m_surface;
};
