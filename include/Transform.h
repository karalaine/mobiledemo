#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class Transform
{
public:
	explicit Transform(Transform* parent = nullptr);
	Transform(glm::mat4 transform, Transform* parent = nullptr);
	void SetPosition(const glm::vec3& pos);
	glm::vec3 GetPosition() const;

	void SetRotation(const glm::vec3& rot);
	glm::vec3 GetRotation() const;

	void SetScale(const glm::vec3& scale);
	glm::vec3 GetScale();

	void Translate(const glm::vec3& delta, bool local = true);
	void Rotate(const glm::vec3& delta, float amount);
	void Scale(const glm::vec3& delta);
	void SetParent(Transform* parent);
	glm::mat4 GetWorldMatrix();
private:
	Transform* m_parent;
	glm::mat4 m_localMatrix;

	glm::vec3 m_position;
	glm::quat m_rotation;
	glm::vec3 m_scale;
	bool m_transformDirty;
};

