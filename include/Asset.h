#pragma once
#include <istream>
#include <memory>

class Asset
{
public:
	Asset() {};
	virtual std::unique_ptr<char[]> GetBuffer() = 0;
	virtual long GetLength() = 0;
	virtual int GetFileDescpritor() = 0;
	virtual bool IsValid() = 0;
	virtual size_t Read(char * buf, size_t pCount) = 0;
	virtual int Seek(size_t pOffset, std::ios_base::seekdir = std::ios::beg) = 0;
	virtual size_t Tell() = 0;
private:
	Asset(const Asset&) = delete;
	Asset& operator=(const Asset&) = delete;

};
