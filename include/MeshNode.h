#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <string>
#include "Node.h"

struct aiNode;

class MeshNode : public Node
{
public:
	MeshNode(std::vector<std::string>& meshGuids);
	MeshNode(aiNode* node, std::vector<std::string>& meshGuids);
	virtual void InitNode() override;
	virtual void CalcTangents();
	virtual void RenderOpaque() override;
        virtual void RenderTransparent() override;
        virtual void CollectTransparent(std::vector<Node *>& transparentList) override;
	virtual void RenderDepth(Shader& depthShader) override;
private:
	std::vector<std::string> m_meshIds;
        void Render(bool skipTransParent = false);
	bool m_hasTangents;
};