#pragma once
#include "Shader.h"
#include <assimp/material.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <unordered_map>
#include <vector>

enum class TextureType { Diffuse, Specular, AlphaMask, Normal };
class Material {
  public:
    Material(const aiMaterial*);
    float* GetDiffuseData() { return glm::value_ptr(m_diffuse); }
    float* GetSpecularData() { return glm::value_ptr(m_specular); }
    float* GetAmbientData() { return glm::value_ptr(m_ambient); }
    std::string& GetTexture(TextureType type) { return m_texturePaths[type]; };
    void SetTexture(TextureType type, const std::string& texturePath) {
        m_texturePaths[type] = texturePath;
    }
	void SetShaderName(const std::string& shaderName) {
        m_shaderName = shaderName;
	}
    unsigned int GetShaderProgram();
    bool HasTexture(TextureType type) {
        return m_texturePaths.find(type) != m_texturePaths.end();
    }
    void LoadAllTextures();

  private:
    glm::vec3 m_diffuse;
    glm::vec3 m_specular;
    glm::vec3 m_ambient;
    std::unordered_map<TextureType, std::string> m_texturePaths;
    std::string m_shaderName;
};
