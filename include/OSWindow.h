//
// Copyright (c) 2014 The ANGLE Project Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//

#ifndef SAMPLE_UTIL_WINDOW_H
#define SAMPLE_UTIL_WINDOW_H

#include <list>
#include <stdint.h>
#include <string>

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "Event.h"

class OSWindow
{
  public:
    OSWindow();
    virtual ~OSWindow();

    virtual bool initialize(const std::string &name, size_t width, size_t height) = 0;
    virtual void destroy() = 0;

    int getX() const;
    int getY() const;
    int getWidth() const;
    int getHeight() const;


    virtual EGLNativeWindowType getNativeWindow() const {
        return mNativeWindow;
    }

    virtual EGLNativeDisplayType getNativeDisplay() const {
        return mNativeDisplay;
    }

    virtual void messageLoop() = 0;

    bool popEvent(Event *event);
    virtual void pushEvent(Event event);

    virtual bool setPosition(int x, int y) = 0;
    virtual bool resize(int width, int height) = 0;
    virtual void setVisible(bool isVisible) = 0;
  protected:
    int mX;
    int mY;
    int mWidth;
    int mHeight;
    EGLNativeWindowType mNativeWindow;
    EGLNativeDisplayType mNativeDisplay;
    std::list<Event> mEvents;
};

OSWindow *CreateOSWindow();

#endif // SAMPLE_UTIL_WINDOW_H
