#pragma once
#include "Node.h"
#include "Shader.h"
#include <stb_truetype.h>

class TextNode : public Node {
  public:
    TextNode(Shader&);
    // Inherited via Node
    virtual void InitNode() override;
    virtual void RenderOpaque() override;
    virtual void RenderDepth(Shader&) override {}
    void SetText(const std::string&);

  private:
    Shader m_shader;
    std::string m_text;
    GLuint m_fontTextureId;
    GLsizei m_indexElementCount;
    std::vector<stbtt_packedchar> m_charData;

    GLuint m_vertexBuffer;
    GLuint m_indexBuffer;
};