#pragma once
#include <string>
#include <vector>
struct aiScene;
namespace Utils {
std::string CreateGUID();
std::vector<std::string> CreateGUID(int i);
std::vector<std::string> LoadAssimpTextures(const aiScene* scene);
std::vector<std::string> LoadAssimpMeshes(const aiScene* scene,
                                          const std::vector<std::string>& materialIds);
}
