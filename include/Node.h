#pragma once
#ifdef USE_GLAD
#include <glad/glad.h>
#else
#include <GLES3/gl3.h>
#endif
#include <glm/glm.hpp>
#include <map>
#include <string>
#include <vector>

#include "Transform.h"
class Shader;
class Node {
public:
  explicit Node(int vaoCount = 0);
    Node(glm::mat4 transform, int vaoCount = 0);
    virtual ~Node();
    virtual void InitNode() = 0;
    virtual void CollectTransparent(std::vector<Node*>& transparentList) {}
    virtual void RenderOpaque() = 0;
    virtual void RenderTransparent() {};
    virtual void RenderDepth(Shader& depthShader) = 0;
    virtual void UseProgram(GLuint program);
    Node* FindChildNode(const std::string& name) const;
    void AddChild(Node* node, const std::string& name);

    Transform& GetTransform() { return m_transform; }

    virtual void Update(){};

  protected:
    std::map<std::string, Node*> m_children;
    Transform m_transform;
    std::vector<GLuint> m_vaos;
    GLuint m_programID;
};
