#pragma once
#include "Camera.h"
#include "Mesh.h"
#include "Node.h"
#include "Shader.h"
#include "Texture.h"

class CubeMap :
	public Node
{
public:
	CubeMap(Camera& cameraRef, Shader& shader);

	// Inherited via Node
	virtual void InitNode() override;
	virtual void RenderOpaque() override;
	virtual void RenderDepth(Shader&) override {}
private:
	Camera& m_camera;
	Mesh m_cubeMesh;
	Shader m_shader;
	Texture m_texture;
    GLuint m_programID;
};

