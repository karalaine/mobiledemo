#pragma once
#include <string>
#include <memory>
#include <fstream>

#include "Asset.h"

class AssetManager
{
public:
	AssetManager();
	~AssetManager();
	virtual Asset& Open(const std::string& filename, 
		std::ios_base::openmode mode = std::ios_base::in);
	virtual void Close(const std::string& filename);
	virtual char GetOSSeparator();

private:
	AssetManager(const AssetManager&) = delete;
	class Impl;
	std::unique_ptr<Impl> _Impl;
};
