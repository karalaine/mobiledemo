#pragma once
#include "Transform.h"
#include "glm/glm.hpp"
class Light
{
public:
	enum class Type
	{
		Directional,
		Point,
		Spot
	};
	Light();
	explicit Light(Type type);
	Transform& GetTransform();
	glm::vec3 GetColor();
	glm::mat4 GetSpaceMatrix();
	Type GetType();
	//Expects normalized color
	void SetColor(glm::vec3);
	//Expects colors from 0-255 range
	void SetColor(unsigned char r, unsigned char g, unsigned char b);
private:
	Transform m_transform;
	glm::vec3 m_color;
	Type m_type;
};