#pragma once
/**
* Basic camera class.
*/
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

class Camera
{
public:
	enum class Movement {
		FORWARD,
		BACKWARD,
		LEFT,
		RIGHT
	};

	Camera();
	Camera(int screenWidth, int screenHeight);

	void SetViewport(int x, int y, int width, int height);
	glm::vec4 GetViewport() const;

	void SetProjectionPerspective(float fov, float aspectRatio, float zNear, float zFar);
	void SetProjectionOrtho(float top, float left, float bottom, float right, float zNear, float zFar);

	void ApplyViewMatrix();

	void SetPosition(const glm::vec3& pos);
	glm::vec3 GetPosition() const;

	// Translate the camera by some amount. If local is TRUE (default) then the translation should
	// be applied in the local-space of the camera. If local is FALSE, then the translation is 
	// applied in world-space.
	void Translate(const glm::vec3& delta, bool local = true);

	void SetRotation(const glm::vec2& rot);
	glm::vec2 GetRotation() const;

	void Move(Movement direction, float deltaTime);
	void Rotate(float xoffset, float yoffset, bool constrainPitch = true);

	glm::mat4 GetProjectionMatrix();
	glm::mat4 GetViewMatrix();
	
private:
	void UpdateViewMatrix();
	glm::vec4 m_Viewport;

	glm::vec3 m_Position;
	glm::quat m_Rotation;

	glm::mat4 m_ViewMatrix;
	glm::mat4 m_ProjectionMatrix;
	bool m_ViewDirty;
	glm::vec3 m_Front;
	glm::vec3 m_Up;
	glm::vec3 m_Right;
	glm::vec3 m_WorldUp;
	float m_Yaw;
	float m_Pitch;
};
