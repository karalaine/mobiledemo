#pragma once
#include <initializer_list>
#include <string>
#ifdef USE_GLAD
#include <glad/glad.h>
#else
#include <GLES3/gl3.h>
#include <GLES3/gl3platform.h>
#endif
class Texture {
  public:
    Texture() = default;
    explicit Texture(const std::string& fileName);
    Texture(Texture&&) noexcept;
    Texture& operator=(Texture&&) noexcept;
    Texture(const Texture&) = delete;
    Texture& operator=(const Texture&) = delete;

    unsigned int GetId();
    GLenum GetBindTarget() { return m_bindTarget; }
    bool Load();
    void Unload();
    int LevelCount() { return m_levels; };

  private:
    std::string m_fileName;
    int m_width;
    int m_height;
    int m_levels;
    GLenum m_bindTarget;

    unsigned int m_textureID;
};