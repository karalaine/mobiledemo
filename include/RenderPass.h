#pragma once
#ifdef USE_GLAD
#include <glad/glad.h>
#else
#include <GLES3/gl3.h>
#endif
#include "Shader.h"
#include "Resources.h"

//TEMP MOCKUP
class RenderPass
{
  public:
	RenderPass(GLuint colorBuffer) : textureColorbuffer(colorBuffer)
	{
		Shader screenShader(&Resources::Get().Assets(), "screenShader");
		m_screenShader = screenShader;
		GLfloat quadVertices[] = {// Vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
								  // Positions   // TexCoords
								  -1.0f, 1.0f, 0.0f, 1.0f,
								  -1.0f, -1.0f, 0.0f, 0.0f,
								  1.0f, -1.0f, 1.0f, 0.0f,

								  -1.0f, 1.0f, 0.0f, 1.0f,
								  1.0f, -1.0f, 1.0f, 0.0f,
								  1.0f, 1.0f, 1.0f, 1.0f};
		GLuint quadVBO;
		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &quadVBO);
		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid *)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid *)(2 * sizeof(GLfloat)));
		glBindVertexArray(0);
	}
	GLint m_screenShader;
	GLuint quadVAO;
	GLuint textureColorbuffer;
	void Render()
	{

		glUseProgram(m_screenShader);
		glBindVertexArray(quadVAO);
		glDisable(GL_DEPTH_TEST);
		glBindTexture(GL_TEXTURE_2D, textureColorbuffer);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		glBindVertexArray(0);
		glEnable(GL_DEPTH_TEST);
	}
};