#pragma once

#include <vector>

#include "Asset.h"

class StdAsset : public Asset
{
public:
	StdAsset(const std::string & filename, std::ios_base::openmode mode);
  StdAsset(StdAsset&& other) noexcept;
	StdAsset& operator=(StdAsset&& other) = default;
	std::unique_ptr<char[]> GetBuffer() override;
	long GetLength() override;
	int GetFileDescpritor() override;
	virtual bool IsValid() override;

	// Inherited via Asset
	virtual size_t Read(char * buf, size_t pCount) override;
	virtual int Seek(size_t pOffset, std::ios_base::seekdir) override;
	virtual size_t Tell() override;

private:
	StdAsset(const StdAsset&) = delete;
	StdAsset& operator=(const StdAsset&) = delete;
    size_t m_length;
	std::vector<char> m_data;
	std::ifstream m_fileStream;
	// Inherited via Asset
};
