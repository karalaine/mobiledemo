#pragma once
// for native audio
#include <string>
#include <memory>

#include "AssetManager.h"

class Audio
{
public:
	Audio();
	virtual ~Audio();
	virtual void CreateEngine();
	virtual bool Open(const std::string & fileName, AssetManager & mgr);
	virtual void Pause();
	virtual void Play();
	virtual int GetPositionInMillis();
	virtual void SetPositionInMillis(int pos);
	virtual bool IsPlaying();
private:
	Audio(const Audio&) = delete;
	class Impl;
	std::unique_ptr<Impl> m_impl;
};
