#pragma once
#include <vector>
#include <array>
#include <memory>
#ifdef USE_GLAD
#include <glad/glad.h>
#else
#include <GLES3/gl3.h>
#endif
#include "Light.h"

class Renderer;
class Node;
class Transform;
struct aiScene;

class Scene
{
  public:
    explicit Scene(bool useDepthmap = false);
	//Scene(const aiScene *scene, std::vector<std::string> meshIds);
	void InitializeScene();

	// Initialize Scene
	virtual void Render();
	void RenderDepthMap();
	void GenerateDepthFBO();
	GLuint GetDepthMap() const;
	// Render the Nodes
	void ClearNodes(); // Remove nodes
	void AddNode(std::shared_ptr<Node> node);
	void AddLight(Light &&light);
	std::vector<Light> &GetLights();
	Renderer *GetRenderer(); // Get scene's renderer
	void ImportAssimpScene(const aiScene *scene);

  private:
	std::vector<std::shared_ptr<Node>> m_nodes;
	std::vector<Light> m_lights;
	std::array<GLuint, 2> m_depthMapFBO;
	std::array<GLuint, 2> m_depthMapTexture;
	// Node's List
	Renderer *renderManager;
	// Scene's Renderer
};
