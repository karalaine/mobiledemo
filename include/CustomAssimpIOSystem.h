#pragma once
#include "AssetManager.h"
#include <assimp/IOSystem.hpp>


class CustomAssimpIOSystem : public Assimp::IOSystem
{
public:
	CustomAssimpIOSystem(AssetManager&);
	// Inherited via IOSystem
	virtual bool Exists(const char * pFile) const override;
	virtual char getOsSeparator() const override;
	virtual Assimp::IOStream * Open(const char * pFile, const char * pMode = "rb") override;
	virtual void Close(Assimp::IOStream * pFile) override;
private:
	AssetManager& m_assetManager;
};
