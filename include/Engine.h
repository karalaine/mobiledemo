#pragma once
#include "Audio.h"
#include "Camera.h"
#include "Renderer.h"
#include "SyncDevice.h"
#include "keyboard.h"
#include "TextNode.h"
#include <set>
/**
 * Our saved state data.
 */
struct saved_state {
};
struct ANativeActivity;

class Engine {

  public:
    Engine();
    void Initialize(EGLNativeWindowType window);
    void Update();
    void DisplayTerminated();
    void Input(int8_t x, int8_t y);
    void KeyPressed(Key key);
    void KeyReleased(Key key);
    bool IsKeyPressed(Key key);
    void Resize(int w, int h);
    Audio& getAudio();
    Renderer& GetRenderer();
    saved_state state;
    int animating;
    static ANativeActivity* androidActivity;

  private:
    std::set<Key> m_pressedKeys;
    int32_t m_width;
    int32_t m_height;
    Audio m_audio;
    Renderer m_renderer;
    Camera m_mainCamera;
    rocket::SyncDevice m_syncDevice;
    std::shared_ptr<TextNode> m_fpsCounter;
    bool m_initialized;
};
