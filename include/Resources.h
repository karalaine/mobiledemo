#pragma once
#include "AssetManager.h"
#include "ResourceManager.h"
#include "Material.h"
#include "Texture.h"
#include "Mesh.h"
#include "Shader.h"

class Resources
{
public:
	static Resources& Get()
	{
		static Resources instance;
		return instance;
	}

	ResourceManager<Material>& Materials() {
		return m_materialManager;
	};
	ResourceManager<Texture>& Textures() {
		return m_textureManager;
	}
	ResourceManager<Mesh>& Meshes() {
		return m_meshManager;
	}
	ResourceManager<Shader>& Shaders() {
		return m_shaderManager;
	}
	
	AssetManager& Assets() {
		return m_assetManager;
	}

private:
	Resources() = default;
	AssetManager m_assetManager;
	ResourceManager<Material> m_materialManager;
	ResourceManager<Texture> m_textureManager;
	ResourceManager<Mesh> m_meshManager;
	ResourceManager<Shader> m_shaderManager;
};