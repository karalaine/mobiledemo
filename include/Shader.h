#pragma once
#ifdef USE_GLAD
#include <glad/glad.h>
#else
#include <GLES3/gl3.h>
#endif
#include "AssetManager.h"
#include "Log.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace {

bool validate_program(GLuint program) {
    bool return_status = true;
    GLint status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
        std::cerr << "Failed to link program" << program << std::endl;
        return_status = false;
    }

    glValidateProgram(program);
    glGetProgramiv(program, GL_VALIDATE_STATUS, &status);
    if (status == GL_FALSE) {
        std::cerr << "Failed to validate program" << program << std::endl;
        return_status = false;
    }

    if (return_status == false) {
        GLint bufferSize = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &bufferSize);
        if (bufferSize != 0) {
            std::string buffer;
            buffer.resize(bufferSize);
            glGetProgramInfoLog(program, bufferSize, &bufferSize, &buffer[0]);
            LOGW("%s", buffer.c_str());
        }
    }
    return return_status;
}
} // namespace
class Shader {
    GLuint m_prog;
    std::string m_fileName;
    AssetManager* m_assetMgr;
    bool m_dirty;
    GLuint compile(GLuint type, const GLchar* source) {
        GLuint shader = glCreateShader(type);
        glShaderSource(shader, 1, &source, NULL);
        glCompileShader(shader);
        GLint compiled;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint length;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
            std::string log(length, ' ');
            glGetShaderInfoLog(shader, length, &length, &log[0]);
            LOGW("Error compiling % shader % %", type == GL_VERTEX_SHADER? "Vertex" : "Fragment",
                    m_fileName, log.c_str());
            return false;
        }
        return shader;
    }

  public:
    Shader() : m_prog(0), m_fileName(), m_assetMgr(nullptr), m_dirty(false) {}

    Shader(AssetManager* mgr, const std::string& fileName)
        : m_prog(0), m_fileName(fileName), m_assetMgr(mgr), m_dirty(false) {
        Compile();
    }
    void Compile() {
        auto& v_source = m_assetMgr->Open(m_fileName + ".vsh");
        auto& f_source = m_assetMgr->Open(m_fileName + ".fsh");
        GLuint vertex_shader, fragment_shader;
        vertex_shader = compile(GL_VERTEX_SHADER, v_source.GetBuffer().get());
        fragment_shader =
            compile(GL_FRAGMENT_SHADER, f_source.GetBuffer().get());
        m_assetMgr->Close(m_fileName + ".vsh");
        m_assetMgr->Close(m_fileName + ".fsh");
        GLint previousProgram;
        glGetIntegerv(GL_CURRENT_PROGRAM, &previousProgram);
        GLuint currentProgram = previousProgram;
        if (m_prog != 0 && m_prog == currentProgram) {
            glUseProgram(0);
        } else {
            m_prog = glCreateProgram();
        }

        glAttachShader(m_prog, vertex_shader);
        glAttachShader(m_prog, fragment_shader);
        glLinkProgram(m_prog);
        GLint isLinked = 0;
        glGetProgramiv(m_prog, GL_LINK_STATUS, &isLinked);
        if (isLinked == GL_FALSE) {
            GLint maxLength = 0;
            glGetProgramiv(m_prog, GL_INFO_LOG_LENGTH, &maxLength);

            // The maxLength includes the NULL character
            std::string infoLog(maxLength, ' ');
            glGetProgramInfoLog(m_prog, maxLength, &maxLength, &infoLog[0]);
            LOGW("Error linking shader % %", m_fileName, infoLog.c_str());
            // The program is useless now. So delete it.
            glDeleteProgram(m_prog);

           m_prog = 0;
        }

        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);
        if (m_prog == currentProgram)
            glUseProgram(currentProgram);
    }
    bool Validate() { return validate_program(m_prog); }
    operator GLuint() {
        if (m_dirty) {
            Compile();
            m_dirty = false;
        }
        return m_prog;
    }
    void operator()() { glUseProgram(m_prog); }
    void SetDirty() { m_dirty = true; }
    ~Shader() {
        //	glDeleteProgram(prog);
    }
};
