#include "Camera.h"

#ifdef USE_GLAD
#include <glad/glad.h>
#else
#define GL_GLEXT_PROTOTYPES 1
#include <GLES3/gl3.h>
#endif

namespace
{
const GLfloat YAW = -90.0f;
const GLfloat PITCH = 0.0f;
} // namespace

Camera::Camera()
	: m_Viewport(0), m_Position(0), m_Rotation(), m_ProjectionMatrix(1), m_ViewMatrix(1), m_ViewDirty(false), m_Up(0.0f, 1.0f, 0.0f), m_Yaw(YAW), m_Pitch(PITCH), m_Front(glm::vec3(0.0f, 0.0f, -1.0f)), m_WorldUp(m_Up)
{
}

Camera::Camera(int screenWidth, int screenHeight)
	: m_Viewport(0, 0, screenWidth, screenHeight), m_Position(0), m_Rotation(), m_ProjectionMatrix(1), m_ViewMatrix(1), m_ViewDirty(false), m_Up(0.0f, 1.0f, 0.0f), m_Yaw(YAW), m_Pitch(PITCH), m_Front(glm::vec3(0.0f, 0.0f, -1.0f)), m_WorldUp(m_Up)
{
}

void Camera::SetViewport(int x, int y, int width, int height)
{
	m_Viewport = glm::vec4(x, y, width, height);
	glViewport(x, y, width, height);
}

glm::vec4 Camera::GetViewport() const
{
	return m_Viewport;
}

void Camera::SetProjectionPerspective(float fov, float aspectRatio, float zNear, float zFar)
{
	m_ProjectionMatrix = glm::perspective(glm::radians(fov), aspectRatio, zNear, zFar);
}

void Camera::SetProjectionOrtho(float top, float left, float bottom, float right, float zNear, float zFar)
{
	m_ProjectionMatrix = glm::ortho(left, right, bottom, top, zNear, zFar);
}

void Camera::ApplyViewMatrix()
{
	UpdateViewMatrix();
}

void Camera::SetPosition(const glm::vec3 &pos)
{
	m_Position = pos;
	m_ViewDirty = true;
}

glm::vec3 Camera::GetPosition() const
{
	return m_Position;
}

void Camera::Translate(const glm::vec3 &delta, bool local /* = true */)
{
	if (local)
	{
		m_Position += m_Rotation * delta;
	}
	else
	{
		m_Position += delta;
	}
	m_ViewDirty = true;
}

glm::vec2 Camera::GetRotation() const
{
	return glm::vec2(m_Yaw, m_Pitch);
}

void Camera::SetRotation(const glm::vec2 &eulerAngles)
{
	m_Yaw = eulerAngles.x;
	m_Pitch = eulerAngles.y;
	m_ViewDirty = true;
}

glm::mat4 Camera::GetProjectionMatrix()
{
	return m_ProjectionMatrix;
}

glm::mat4 Camera::GetViewMatrix()
{
	UpdateViewMatrix();
	return m_ViewMatrix;
}

void Camera::Move(Movement direction, float deltaTime)
{
	GLfloat velocity = 3.0f * deltaTime;
	if (direction == Movement::FORWARD)
		m_Position += m_Front * velocity;
	if (direction == Movement::BACKWARD)
		m_Position -= m_Front * velocity;
	if (direction == Movement::LEFT)
		m_Position -= m_Right * velocity;
	if (direction == Movement::RIGHT)
		m_Position += m_Right * velocity;

	m_ViewDirty = true;
}

// Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
void Camera::Rotate(float xoffset, float yoffset, bool constrainPitch /* = true */)
{
	m_Yaw += xoffset;
	m_Pitch += yoffset;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (constrainPitch)
	{
		if (m_Pitch > 89.0f)
			m_Pitch = 89.0f;
		if (m_Pitch < -89.0f)
			m_Pitch = -89.0f;
	}
	m_ViewDirty = true;
}

void Camera::UpdateViewMatrix()
{
	if (m_ViewDirty)
	{
		glm::vec3 front;
		front.x = cos(glm::radians(m_Yaw)) * cos(glm::radians(m_Pitch));
		front.y = sin(glm::radians(m_Pitch));
		front.z = sin(glm::radians(m_Yaw)) * cos(glm::radians(m_Pitch));
		m_Front = glm::normalize(front);
		// Also re-calculate the Right and Up vector
		m_Right = glm::normalize(glm::cross(m_Front, m_WorldUp)); // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		m_Up = glm::normalize(glm::cross(m_Right, m_Front));
		m_ViewMatrix = glm::lookAt(m_Position, m_Position + m_Front, m_Up);
		m_ViewDirty = false;
	}
}
