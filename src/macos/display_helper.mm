#include <EGL/eglplatform.h>
#define GLFW_EXPOSE_NATIVE_COCOA
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

EGLNativeWindowType glfwGetNativeHandle(GLFWwindow* handle) {
    NSWindow* window = glfwGetCocoaWindow(handle);
    NSView* view = window.contentView;
    [view setWantsLayer:YES];
    return [view layer];
}
