#include <assimp/IOStream.hpp>
#include "CustomAssimpIOSystem.h"


class CustomAssimpIOStream : public Assimp::IOStream
{
public:
	CustomAssimpIOStream(Asset& asset, const std::string&& fileName)
		: m_asset(asset),
		m_fileName(fileName)
	{

	}

	size_t Read(void * pvBuffer, size_t pSize, size_t pCount) override
	{
		return m_asset.Read(static_cast<char*>(pvBuffer), pSize*pCount);
	}

	size_t Write(const void *, size_t, size_t) override
	{
		//Supporting write only
		return 0;
	}

	aiReturn Seek(size_t pOffset, aiOrigin pOrigin) override
	{
		std::ios_base::seekdir dir = std::ios::beg;
		switch (pOrigin)
		{
		case aiOrigin_SET:
			dir = std::ios::beg;
			break;
		case aiOrigin_CUR:
			dir = std::ios::cur;
			break;
		case aiOrigin_END:
			dir = std::ios::end;
			break;
		case _AI_ORIGIN_ENFORCE_ENUM_SIZE:
		default:
			break;
		}
		int success = m_asset.Seek(pOffset, dir);
		return  success == 0 ? aiReturn_SUCCESS : aiReturn_FAILURE;
	}
	size_t Tell() const override
	{
		return m_asset.Tell();
	}
	size_t FileSize() const override
	{
		return m_asset.GetLength();
	}
	void Flush() override
	{
		//Supporting write only
	}
	const std::string& FileName()
	{
		return m_fileName;
	}

private:
	Asset& m_asset;
	const std::string m_fileName;
};


CustomAssimpIOSystem::CustomAssimpIOSystem(AssetManager & mgr)
	: m_assetManager(mgr)
{
}

bool CustomAssimpIOSystem::Exists(const char * pFile) const
{
	auto fileName = std::string(pFile);
	auto& asset = m_assetManager.Open(fileName);
	bool exists = asset.IsValid();
	m_assetManager.Close(fileName);
	return exists;
}

char CustomAssimpIOSystem::getOsSeparator() const
{
	return 0;
}

Assimp::IOStream * CustomAssimpIOSystem::Open(const char * pFile, const char * pMode)
{
	std::ios::openmode mode;
	// "rb", "r", "rt".
	if (strcmp(pMode, "rb") == 0)
	{
		mode = std::ios::in | std::ios::binary;
	}
	else if (strcmp(pMode, "r") == 0 || strcmp(pMode, "rt") == 0)
	{
		mode = std::ios::in;
	}
	else
	{
		//Not supporting write modes
		return nullptr;
	}
	return new CustomAssimpIOStream(m_assetManager.Open(pFile, mode), pFile);
}

void CustomAssimpIOSystem::Close(Assimp::IOStream * pFile)
{
	m_assetManager.Close(dynamic_cast<CustomAssimpIOStream*>(pFile)->FileName());
}
