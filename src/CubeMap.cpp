#include "CubeMap.h"
#include "Resources.h"
#include "Texture.h"
//#include <assimp\scene.h>
//#include <assimp\importer.hpp>

CubeMap::CubeMap(Camera& cameraRef, Shader& shader)
    : m_camera(cameraRef), m_shader(shader), m_programID() {}

void CubeMap::InitNode() {
    m_texture = Texture("hd_lake_uncompressed.ktx");
    m_texture.Load();
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_texture.GetId());
#ifdef GL_TEXTURE_CUBE_MAP_SEAMLESS
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
#endif //  GL_TEXTURE_CUBE_MAP_SEAMLESS
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    GLfloat skyboxVertices[] = {
        // Positions  Front
        1.0, 1.0, 1.0,   // Right Top
        1.0, -1.0, 1.0,  // Right Bottom
        -1.0, -1.0, 1.0, // Left Bottom
        -1.0, 1.0, 1.0,  // Left Top
        //
        1.0, 1.0, -1.0,   // Right Top
        1.0, -1.0, -1.0,  // Right Bottom
        -1.0, -1.0, -1.0, // Left Bottom
        -1.0, 1.0, -1.0,  // Left Top
    };

    GLushort skyboxIndices[] = {
        // front
        0,
        1,
        2,
        2,
        3,
        0,
        // top
        1,
        5,
        6,
        6,
        2,
        1,
        // back
        7,
        6,
        5,
        5,
        4,
        7,
        // bottom
        4,
        0,
        3,
        3,
        7,
        4,
        // left
        4,
        5,
        1,
        1,
        0,
        4,
        // right
        3,
        2,
        6,
        6,
        7,
        3,
    };
    m_vaos.resize(1);
    GLuint vbo, ebo;
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);
    glGenVertexArrays(1, &m_vaos[0]);
    glBindVertexArray(m_vaos[0]);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices,
                 GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat),
                          (GLvoid*)0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(skyboxIndices), &skyboxIndices,
                 GL_STATIC_DRAW);

    glBindVertexArray(0);
}

void CubeMap::RenderOpaque() {
    // Draw skybox as last
    glDepthFunc(GL_LEQUAL); // Change depth function so depth test passes when
                            // values are equal to depth buffer's content
    m_shader();
    auto uniProjection = glGetUniformLocation(m_shader, "projection");
    glUniformMatrix4fv(uniProjection, 1, GL_FALSE,
                       glm::value_ptr(m_camera.GetProjectionMatrix()));

    glm::mat4 view = glm::mat4(
        glm::mat3(m_camera.GetViewMatrix())); // Remove any translation
                                              // component of the view matrix
    glUniformMatrix4fv(glGetUniformLocation(m_shader, "view"), 1, GL_FALSE,
                       glm::value_ptr(view));
    // skybox cube
    glBindVertexArray(m_vaos[0]);
    glActiveTexture(GL_TEXTURE0);
    int size;
    glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
    glUniform1i(glGetUniformLocation(m_shader, "skybox"), 0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_texture.GetId());
    glDrawElements(GL_TRIANGLES, size / sizeof(GLushort), GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    glDepthFunc(GL_LESS); // Set depth function back to default*/
}
