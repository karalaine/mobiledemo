#include "Node.h"

Node::Node(int vaoCount) : m_vaos(vaoCount) {}

Node::Node(glm::mat4 transform, int vaoCount)
    : m_transform(transform), m_vaos(vaoCount) {}

Node::~Node() {}

void Node::UseProgram(GLuint program) { m_programID = program; }

Node* Node::FindChildNode(const std::string&) const { return nullptr; }

void Node::AddChild(Node* node, const std::string& name) {
    m_children[name] = node;
    node->m_transform.SetParent(&m_transform);
}