#include "Texture.h"
#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_JPEG
#define STBI_ONLY_PNG
#define STBI_ONLY_TGA
#include "Resources.h"
#include "ktx.h"
#include "stb_image.h"

Texture::Texture(const std::string& fileName)
    : m_fileName(fileName), m_width(0), m_height(0), m_bindTarget(0),
      m_textureID(0), m_levels(1) {}


Texture::Texture(Texture&& other) noexcept
    : m_fileName(other.m_fileName), m_width(other.m_width),
      m_height(other.m_height), m_textureID(other.m_textureID),
      m_bindTarget(other.m_bindTarget), m_levels(other.m_levels) {}

Texture& Texture::operator=(Texture&& other) noexcept {
    m_levels = other.m_levels;
    m_bindTarget = other.m_bindTarget;
    m_fileName = other.m_fileName;
    m_width = other.m_width;
    m_height = other.m_height;
    m_textureID = other.m_textureID;
    return *this;
}

GLuint Texture::GetId() { return m_textureID; }

bool Texture::Load() {
    if (m_textureID != 0)
        return true; // Already loaded

    AssetManager& assetManager = Resources::Get().Assets();
    auto& asset = assetManager.Open(m_fileName,
                                    std::ios_base::in | std::ios_base::binary);
    GLboolean isMipmapped = false;
    glGenTextures(1, &m_textureID);
    // First try to load as packed image
    // auto err = ktxLoadTextureM(asset.GetBuffer().get(), asset.GetLength(),
    //                            &m_textureID, &m_bindTarget, &dimens,
    //                            &isMipmapped, nullptr, nullptr, nullptr);
    ktxTexture* texture;
#pragma warning(disable : 26812)
    auto result = ktxTexture_CreateFromMemory(
        reinterpret_cast<ktx_uint8_t*>(asset.GetBuffer().get()),
        asset.GetLength(),
        KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT |
            KTX_TEXTURE_CREATE_SKIP_KVDATA_BIT,
        &texture);

#pragma warning(default : 26812)
    if (result != KTX_error_code_t::KTX_SUCCESS) {
#if defined(STB_IMAGE_IMPLEMENTATION)
        stbi_io_callbacks cb{
            [](void* user, char* data, int size) {
                auto asset = static_cast<Asset*>(user);
                auto rd = asset->Read(static_cast<char*>(data), size);
                return static_cast<int>(rd); // read from file
            },
            [](void* user, int n) {
                auto asset = static_cast<Asset*>(user);
                asset->Seek(static_cast<size_t>(n), std::ios::cur);
            },
            [](void* user) {
                auto asset = static_cast<Asset*>(user);
                return asset->Tell() >= asset->GetLength() ? 0 : -1;
            }};
        int comp;
        asset.Seek(0, std::ios::beg);
        auto textureData = stbi_load_from_callbacks(&cb, &asset, &m_width,
                                                    &m_height, &comp, 0);
        assetManager.Close(m_fileName);
        if (textureData == nullptr)
            return false;
        //	throw std::runtime_error("Texture failed to load");
        m_bindTarget = GL_TEXTURE_2D;
        m_levels = 1;
        glBindTexture(GL_TEXTURE_2D, m_textureID);
        // Set the texture wrapping/filtering options (on the currently bound
        // texture object)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                        GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
#ifdef GL_TEXTURE_MAX_ANISOTROPY_EXT
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);
#endif
        GLenum format = 0;
        if (comp == 3) {
            format = GL_RGB;
        } else if (comp == 4) {
            format = GL_RGBA;
        } else {
            LOGW("Unknown amount of color channels %", comp);
            return false;
        }
        glTexImage2D(GL_TEXTURE_2D, 0, format, m_width, m_height, 0, format,
                     GL_UNSIGNED_BYTE, textureData);
        stbi_image_free(textureData);
        glGenerateMipmap(GL_TEXTURE_2D);
#endif
    } else {
        GLenum glErrors = 0;
        result = ktxTexture_GLUpload(texture, &m_textureID, &m_bindTarget,
                                     &glErrors);
        ktxTexture_Destroy(texture);
        if (result != KTX_error_code_t::KTX_SUCCESS || glErrors > 0) {
            return false;
        }
        m_width = texture->baseWidth;
        m_height = texture->baseHeight;
        if (texture->numDimensions > 0) {
            m_levels = texture->numLayers;
        }
        if (isMipmapped == false) {
            glGenerateMipmap(m_bindTarget);
        }
        assetManager.Close(m_fileName);
        if (m_bindTarget != GL_TEXTURE_CUBE_MAP) {
            glTexParameteri(m_bindTarget, GL_TEXTURE_MIN_FILTER,
                            GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(m_bindTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
#ifdef GL_EXT_texture_filter_anisotropic
            glTexParameterf(m_bindTarget, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);
#endif
        }
        return true;
    }
    return true;
}

void Texture::Unload() { 
	glDeleteTextures(1, &m_textureID); 
	m_textureID = 0;
}

