#include "LoaderBar.h"

#include <vector>
#include <math.h>
#include <numeric>

LoaderBar::LoaderBar()
{
}


LoaderBar::~LoaderBar()
{
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
}

void LoaderBar::InitNode()
{
	glUseProgram(m_programID);
	// Query and send the uniform variable.

	positionAttribHandle = glGetAttribLocation(m_programID, "VertexPosition");

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	GLuint EBO;

	glGenBuffers(1, &EBO);
	std::vector<float> vertices;
	std::vector<GLuint> indices(362);
	std::iota(std::begin(indices), std::end(indices), 0);
	vertices.push_back(0);
	vertices.push_back(0);
	vertices.push_back(0);
	const float pi = 3.14159265f;

	for (float i = pi / 2; i < 2 * pi + pi / 2; i += 2 * pi / 360.f)
	{
		vertices.push_back(cos(i));
		vertices.push_back(sin(i));
		vertices.push_back(0);
	}
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vertices.size(), vertices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*indices.size(), indices.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(positionAttribHandle, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(positionAttribHandle);
	glBindVertexArray(0);

	// Bind back to the default state.
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void LoaderBar::RenderOpaque()
{
	static int angle = 0;
	glUseProgram(m_programID);
	angle += 1;
	if (angle > 361)
		angle = 0;

	glBindVertexArray(vao);

	glDrawElements(GL_TRIANGLE_FAN, angle, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

}
