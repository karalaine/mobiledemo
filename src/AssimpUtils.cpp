#include "Log.h"
#include "Resources.h"
#include "Utils.h"
#include <assimp/scene.h>
#include <string>
#include <vector>

namespace {

aiTextureType TextureTypeToaiTextureType(TextureType type) {
    switch (type) {
    case TextureType::Diffuse:
        return aiTextureType_DIFFUSE;
    case TextureType::AlphaMask:
        return aiTextureType_OPACITY;
    case TextureType::Normal:
        return aiTextureType_HEIGHT;
    default:
        return aiTextureType_UNKNOWN;
    }
}

bool LoadAssimpTexture(aiMaterial* assimpMaterial, TextureType type,
                       Resources& resources, std::string& outPath) {

    aiTextureType aitTexType = TextureTypeToaiTextureType(type);

    aiString path; // filename
    if (assimpMaterial->GetTexture(aitTexType, 0, &path) == AI_SUCCESS) {
        outPath = path.data;
        if (!resources.Textures().Get(outPath)) {

            Texture texture(outPath);
            if (!texture.Load()) {
                LOGW("Texture %s failed to load!", path.data);
            } else {
                LOGI("Adding texture from path %s", path.data);
            }

            resources.Textures().Add(outPath, std::move(texture));
        }
        return true;
    }
    return false;
}

} // namespace
std::vector<std::string> Utils::LoadAssimpTextures(const aiScene* scene) {

    std::vector<std::string> materialIds =
        Utils::CreateGUID(scene->mNumMaterials);
    // TODO: load all textures for material
    std::vector<std::string> textureIds;
    auto& resources = Resources::Get();

    for (unsigned int i = 0; i < scene->mNumMaterials; i++) {

        std::string shaderName{"diffuse"};
        Material mat(scene->mMaterials[i]);
        auto assimpMaterial = scene->mMaterials[i];
        std::string outPath;
        if (LoadAssimpTexture(assimpMaterial, TextureType::Diffuse, resources,
                              outPath)) {
            mat.SetTexture(TextureType::Diffuse, outPath);
        } else {
            aiString name;
            assimpMaterial->Get(AI_MATKEY_NAME, name);
            LOGW("Assimp material %s didnt have diffuse texture", name.C_Str());
        }
        if (LoadAssimpTexture(assimpMaterial, TextureType::AlphaMask, resources,
                              outPath)) {
            mat.SetTexture(TextureType::AlphaMask, outPath);
            shaderName = "diffuseAlphamask";
		}
        if (LoadAssimpTexture(assimpMaterial, TextureType::Normal, resources,
                              outPath)) {
            mat.SetTexture(TextureType::Normal, outPath);
        }
        mat.SetShaderName(shaderName);
        resources.Materials().Add(std::string(materialIds[i]), std::move(mat));
    }
    return materialIds;
}

std::vector<std::string>
Utils::LoadAssimpMeshes(const aiScene* scene,
                        const std::vector<std::string>& materialIds) {
    std::vector<std::string> meshIds = Utils::CreateGUID(scene->mNumMeshes);

    for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
        auto assimpMesh = scene->mMeshes[i];
        Resources::Get().Meshes().Add(
            std::string(meshIds[i]),
            Mesh(assimpMesh, materialIds[assimpMesh->mMaterialIndex]));
    }
    return meshIds;
}
