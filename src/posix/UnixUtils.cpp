#include <uuid/uuid.h>

#include "Utils.h"

std::string Utils::CreateGUID()
{
	uuid_t id;
  	uuid_generate(id);
  	return std::string { std::begin(id), std::end(id) };
}

std::vector<std::string> Utils::CreateGUID(int count)
{
	std::vector<std::string> guids(count);
	for (int i = 0; i < count; i++)
	{
		guids[i] = CreateGUID();
	}
	return guids;
}
