#pragma once
#include <iostream>


namespace
{
	const std::string ANSI_COLOR_YELLOW = "\x1b[33m";
	const std::string ANSI_COLOR_RESET = "\x1b[0m";	
} 


inline void LOGI(const char* format)
{
	std::cout << ANSI_COLOR_RESET  << format;
}

inline void LOGW(const char* format)
{
	std::cout << ANSI_COLOR_YELLOW  << format << ANSI_COLOR_RESET;
}

template<typename T, typename... Targs>
inline void LOGI(const char* format, T value, Targs... Fargs) // recursive variadic function
{
	std::cout << ANSI_COLOR_RESET;
	for (; *format != '\0'; format++) {
		if (*format == '%') {
			std::cout << value;
			LOGI(format + 1, Fargs...); // recursive call
			return;
		}
		std::cout << *format;
	}
}

template<typename T, typename... Targs>
inline void LOGW(const char* format, T value, Targs... Fargs)
{
	std::cout << ANSI_COLOR_YELLOW;
	
	for (; *format != '\0'; format++) {
		if (*format == '%') {
			std::cout << value;
			LOGW(format + 1, Fargs...); // recursive call
			return;
		}
		std::cout << *format;
	}
	std::cout << ANSI_COLOR_RESET;

}