#include <map>
#include <fstream>

#include "AssetManager.h"
#include "StdAsset.h"
#include "Resources.h"

class AssetManager::Impl
{
public:

	Impl()		
	{
	
	}

	~Impl()
	{
		
	}
	Asset& Open(const std::string & filename, std::ios_base::openmode mode)
	{
		auto pos = m_assets.find(filename);
		if (pos == m_assets.end())
		{	
			StdAsset asset(filename, mode);

			if (asset.IsValid())
			{
				auto pair = m_assets.emplace(std::string(filename), std::move(asset));
				return pair.first->second;
			}
			throw std::runtime_error("File not found! " + filename);
		}
		else
		{
			return pos->second;
		}
	}

	void Close(const std::string & filename)
	{
		m_assets.erase(filename);

	}
private:
	std::map<std::string, StdAsset> m_assets;
};

AssetManager::AssetManager() :
	_Impl(new Impl())
{}
AssetManager::~AssetManager() = default;

Asset& AssetManager::Open(const std::string & filename, std::ios_base::openmode mode)
{
	return _Impl->Open(filename, mode);
}

void AssetManager::Close(const std::string & filename)
{
	_Impl->Close(filename);
}

char AssetManager::GetOSSeparator()
{
	return '\\';
}
