#define NOMINMAX 1
#include <fstream>
#include <cassert>
#include <iostream>
#include <limits>
#include <algorithm>
#include "Log.h"
#include "StdAsset.h"

StdAsset::StdAsset(const std::string & filename, std::ios_base::openmode mode) :
	m_fileStream(filename, mode)
{

    m_fileStream.ignore( std::numeric_limits<std::streamsize>::max() );
    m_length = m_fileStream.gcount();
    m_fileStream.clear();   //  Since ignore will have set eof.
    m_fileStream.seekg(0, m_fileStream.beg);
    auto mask = std::ifstream::failbit | std::ifstream::badbit | std::ifstream::eofbit;
    m_fileStream.exceptions(mask);
}

StdAsset::StdAsset(StdAsset && other) noexcept
{
	m_fileStream = std::move(other.m_fileStream);
	m_length = other.m_length;
	m_data = std::move(other.m_data);
}

std::unique_ptr<char[]> StdAsset::GetBuffer()
{
	assert(IsValid());
	std::cout << m_fileStream.tellg();
	auto tmp = std::make_unique<char[]>(m_length + 1l);
	Read(tmp.get(), m_length);
	tmp[m_length] = '\0';
	return tmp;
}

long StdAsset::GetLength()
{
	return m_length;
}

int StdAsset::GetFileDescpritor()
{
	return -1;
}

bool StdAsset::IsValid()
{
	return m_fileStream.is_open();
}

size_t StdAsset::Read(char * buf, size_t pCount)
{
	assert(IsValid());
	try
	{
	    auto maxChars = std::max(0uz, std::min(m_length - Tell(), pCount));
		m_fileStream.read(buf, maxChars);
		if (m_fileStream.good())
        {
            return maxChars;
        }
	}
	catch (std::exception& ex)
	{
	    auto failState = m_fileStream.rdstate();
        auto isEof = failState == std::ios_base::eofbit;
	    LOGW("File read exception: % state: % was EOF: %", ex.what(), failState, isEof);
		m_fileStream.clear();
	}
	return static_cast<size_t>(m_fileStream.gcount());
}

int StdAsset::Seek(size_t pOffset, std::ios_base::seekdir dir)
{
	assert(IsValid());
	try
	{
		m_fileStream.seekg(pOffset, dir);
		return 0;
	}
	catch (std::exception&)
	{
		m_fileStream.clear();
		return -1;
	}
}

size_t StdAsset::Tell()
{
	return static_cast<size_t>(m_fileStream.tellg());
}
