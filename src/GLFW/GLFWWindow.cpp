#include "GLFWWindow.h"
#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>


bool GLFWOSWindow::initialize(const std::string& name, size_t width,
                size_t height) {
  GLFWOSWindow* window;

  /* Initialize the library */
  if(!glfwInit())
  {
     return false;
  }         
    return true;
}
void GLFWOSWindow::destroy() {
   glfwDestroyWindow(m_glfWindow);
   glfwTerminate();         
                
}


void GLFWOSWindow::messageLoop() {
   while (!glfwWindowShouldClose(m_glfWindow)) {
        glfwPollEvents();
   }                
}

void GLFWOSWindow::pushEvent(Event event) {
                    
                
}
bool GLFWOSWindow::setPosition(int x, int y) {
   glfwSetWindowPos(m_glfWindow, x, y);        
}
bool GLFWOSWindow::resize(int width, int height) {
   glfwSetWindowSize(m_glfWindow, width, height);
}
void GLFWOSWindow::setVisible(bool isVisible) {
   if(isVisible) {
      glfwShowWindow(m_glfWindow);
   } else {
      glfwHideWindow(m_glfWindow);
   }   
                
}
