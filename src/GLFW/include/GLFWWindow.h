#include <string>
#include "OSWindow.h"

class GLFWwindow;
class GLFWOSWindow : public OSWindow {
public:
    bool initialize(const std::string& name, size_t width,
                    size_t height) override;
    void destroy() override;

    void messageLoop() override;

    void pushEvent(Event event) override;
    bool setPosition(int x, int y) override;
    bool resize(int width, int height) override;
    void setVisible(bool isVisible) override;
private:
    GLFWwindow* m_glfWindow;
};