/**
 * Shared state for our app.
 */
#include <vector>

#ifdef USE_GLAD
#include <glad/glad.h>
#else
#include <GLES3/gl3.h>
#endif
#include "CubeMap.h"
#include "CustomAssimpIOSystem.h"
#include "Engine.h"
#include "LoaderBar.h"
#include "Log.h"
#include "MeshNode.h"
#include "RenderPass.h"
#include "Resources.h"
#include "Scene.h"
#include "Shader.h"
#include "TextNode.h"
#include "TriangleModel.h"
#include "Utils.h"
#include <assimp/DefaultLogger.hpp>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
/**
 * Just the current frame in the display.
 */

static void check_error() {
    for (GLint error = glGetError(); error; error = glGetError()) {
        LOGI("glError (0x%x)\n", error);
    }
}
Engine::Engine()
    : animating(0), window(), m_width(0), m_height(0), 
      m_syncDevice("sync", 255), m_initialized() {}

const static std::string host = "192.168.2.9";

void Engine::Initialize(EGLNativeWindowType window) {
    m_renderer.InitDisplay(window);
    // m_syncDevice.Connect(host, 1338);
    auto& resources = Resources::Get();
    auto& assetManager = resources.Assets();
    m_audio.CreateEngine();
    m_audio.Open("tune.ogg", assetManager);
    m_mainCamera.Translate(glm::vec3(0.0f, 10.f, 1.0f));
    Resize(m_renderer.GetWidth(),
           m_renderer.GetHeight()); // Ensure at least one resize call as window
                                    // event might be delayed
    Assimp::Importer importer{};
    Assimp::DefaultLogger::create("", Assimp::Logger::NORMAL,
                                  aiDefaultLogStream_STDOUT);
    // Assimp will take ownership of the IO handler
    importer.SetIOHandler(new CustomAssimpIOSystem(assetManager));

    auto duck =
        importer.ReadFile("duck.fbx", aiProcessPreset_TargetRealtime_Fast);
    auto scene = std::make_unique<Scene>(true);
    //  auto mirrorMaterialIds = Utils::CreateGUID(1);
    auto duckMaterial = Utils::LoadAssimpTextures(duck);
    auto meshIds = Utils::LoadAssimpMeshes(duck, duckMaterial);
    auto duckNode = std::make_shared<MeshNode>(duck->mRootNode, meshIds);
    auto& duckTransform = duckNode->GetTransform();
    duckTransform.Scale(glm::vec3(0.005f));
    duckTransform.SetPosition(glm::vec3(0, 0, 0));
    scene->AddNode(duckNode);
    /* auto planeVertices =
        std::vector<Vertex>{{glm::vec3(-1, 0, -1), glm::vec3(0, 1, 0),
                             glm::vec2(0, 0), glm::vec3(), glm::vec3()},
                            {glm::vec3(-1, 0, 1), glm::vec3(0, 1, 0),
                             glm::vec2(0, 20), glm::vec3(), glm::vec3()},
                            {glm::vec3(1, 0, -1), glm::vec3(0, 1, 0),
                             glm::vec2(20, 0), glm::vec3(), glm::vec3()},
                            {glm::vec3(1, 0, 1), glm::vec3(0, 1, 0),
                             glm::vec2(20, 20), glm::vec3(), glm::vec3()}};
    auto planeIndices = std::vector<unsigned int>{0, 1, 2, 1, 3, 2};

    Texture ground("rocks.ktx");
    ground.Load();
    std::string shaderName = ground.GetShaderName();

    auto& groundShader =
        *resources.Shaders().GetOrAdd(shaderName, resources.Assets());
    groundShader();
    GLint uniTextureArrayLoc =
        glGetUniformLocation(groundShader, "TextureArray");
    if (uniTextureArrayLoc != -1) {
        glUniform1i(uniTextureArrayLoc, 0);
        GLint shadowMapLoc = glGetUniformLocation(groundShader, "shadowMap");
        if (shadowMapLoc != -1)
            glUniform1i(shadowMapLoc, 1);
    }
    resources.Textures().Add("planeTex", std::move(ground));
    resources.Materials().Add(std::string{"planeMat"},
                              Material(nullptr, "planeTex", groundShader));
    resources.Meshes().Add("plane", Mesh(std::move(planeVertices),
                                         std::move(planeIndices), "planeMat"));
    std::vector<std::string> planeId{"plane"};
    auto planeNode = std::make_shared<MeshNode>(planeId);
    planeNode->CalcTangents();
    planeNode->GetTransform().Scale(glm::vec3(5, 1, 5));
    // scene->AddNode(planeNode);
        */
    auto sponzaImported =
        importer.ReadFile("sponza.obj", aiProcessPreset_TargetRealtime_Fast |
                                            aiProcess_FindInstances |
                                            aiProcess_RemoveRedundantMaterials |
                                            aiProcess_PreTransformVertices);
    auto sponzaMat = Utils::LoadAssimpTextures(sponzaImported);
    auto sponzaMeshes = Utils::LoadAssimpMeshes(sponzaImported, sponzaMat);
    auto sponzaNode =
        std::make_shared<MeshNode>(sponzaImported->mRootNode, sponzaMeshes);
    sponzaNode->GetTransform().Scale(glm::vec3(0.005f));
    scene->AddNode(sponzaNode);
    
    /// LIGHTS
       Light pointL(Light::Type::Point);
       pointL.GetTransform().SetPosition(glm::vec3(10.5f, 5.0f, 0.3f));
       scene->AddLight(std::move(pointL));

       Light pointL2(Light::Type::Point);
       pointL2.GetTransform().SetPosition(glm::vec3(-10.5f, 5.0f, 0.3f));
       pointL2.SetColor(glm::vec3(1, 1, 1));
       scene->AddLight(std::move(pointL2));

       Light pointL3(Light::Type::Point);
       pointL3.GetTransform().SetPosition(glm::vec3(0.3f, 5.0f, -10.5f));
       pointL3.SetColor(glm::vec3(1, 1, 1));
       scene->AddLight(std::move(pointL3));

       Light pointL4(Light::Type::Point);
       pointL4.GetTransform().SetPosition(glm::vec3(0.3f, 5.0f, 10.5f));
       pointL4.SetColor(glm::vec3(1, 1, 1));
       scene->AddLight(std::move(pointL4));
           
    Light directional(Light::Type::Directional);
    // For now, position acts as direction
    directional.GetTransform().SetPosition(glm::vec3(0.0f, 10.f, 1.0f));
    // Overcast color
    directional.SetColor(201, 226, 255);
    scene->AddLight(std::move(directional));
	//load shaders
    auto& shaders = resources.Shaders();
    shaders.Add("diffuse", Shader(&assetManager, "diffuse"));
    shaders.Add("diffuseAlphamask", Shader(&assetManager, "diffuseAlphamask"));
    shaders.Add("depthMap", Shader(&assetManager, "depthMap"));
    auto cubemapSharerIter = shaders.Add("cubemap", Shader(&assetManager, "cubemap"));
    auto textShaderIter = shaders.Add("text", Shader(&assetManager, "text"));
    auto& textShader = textShaderIter.first->second;
    /// INIT
    scene->InitializeScene();
    auto addonScene = std::make_unique<Scene>();

    textShader();
    auto orthoProjection =
        glm::ortho(0.f, (float)m_width, 0.f, (float)m_height);
    auto textProj = glGetUniformLocation(textShader, "projection");
    glUniformMatrix4fv(textProj, 1, GL_FALSE, glm::value_ptr(orthoProjection));

    auto cb =
        std::make_shared<CubeMap>(m_mainCamera, cubemapSharerIter.first->second);
    m_fpsCounter = std::make_shared<TextNode>(textShader);

    addonScene->AddNode(m_fpsCounter);
    addonScene->AddNode(cb);
    addonScene->InitializeScene();

    // LoaderBar *bar = new LoaderBar();
    //	bar->useProgram(exampleShader);
    //	bar->InitNode();
    //	m_mainCamera.SetProjectionOrtho(-2, -2, 2, 2, 0.1f, 100);

    // scene->addNode(bar);
    // m_renderer.AddRenderPass(
    //    std::make_unique<RenderPass>(scene->GetDepthMap(), "screen"));
    m_renderer.AddScene(std::move(scene));
    m_renderer.AddScene(std::move(addonScene));
    m_initialized = true;
}

static double GetRowRate() {
    static const double bpm = 150.0; /* beats per minute */
    static const int rpb = 8;        /* rows per beat */
    return bpm / 60.0 / 1000.0 * rpb;
}

static rocket::SynCb& GetCb() {
    static rocket::SynCb callbacks = {
        [](void* data, bool shouldPause) {
            Audio* audio = static_cast<Audio*>(data);
            shouldPause ? audio->Pause() : audio->Play();
        },
        [](void* data, int row) {
            Audio* audio = static_cast<Audio*>(data);
            audio->SetPositionInMillis(static_cast<int>(row / GetRowRate()));
        },
        [](void* data) {
            Audio* audio = static_cast<Audio*>(data);
            return audio->IsPlaying();
        }};
    return callbacks;
}
#include <chrono>
static std::chrono::high_resolution_clock::time_point prevTime{};
static std::chrono::high_resolution_clock hiResClock{};

void Engine::Update() {
    // int time = m_audio.GetPositionInMillis();
    if(m_initialized == false) {
        return;
    }
    auto time = hiResClock.now();
    std::chrono::duration<double, std::milli> dt = time - prevTime;
    prevTime = time;
    m_fpsCounter->SetText("Frametime: " + std::to_string(dt.count()) + "ms");

    int row = static_cast<int>(1 * GetRowRate());
    const rocket::Track& clear_r = m_syncDevice.GetTrack("clear.r");
    const rocket::Track& clear_b = m_syncDevice.GetTrack("clear.b");
    const rocket::Track& clear_g = m_syncDevice.GetTrack("clear.g");
    const rocket::Track& rotation = m_syncDevice.GetTrack("rotation");
    //	if (!m_syncDevice.Update(row, GetCb(), &m_audio))
    // m_syncDevice.Connect(host, 1338);
    glClearColor(float(clear_r.GetVal(row)), float(clear_b.GetVal(row)),
                 float(clear_g.GetVal(row)), 1.0f);
  
    float deltaTime =
        std::chrono::duration<float>(dt).count();
    if (IsKeyPressed(Key::KEY_A))
        m_mainCamera.Move(Camera::Movement::LEFT, deltaTime);
    if (IsKeyPressed(Key::KEY_D))
        m_mainCamera.Move(Camera::Movement::RIGHT, deltaTime);
    if (IsKeyPressed(Key::KEY_W))
        m_mainCamera.Move(Camera::Movement::FORWARD, deltaTime);
    if (IsKeyPressed(Key::KEY_S))
        m_mainCamera.Move(Camera::Movement::BACKWARD, deltaTime);
    glm::mat4 view = m_mainCamera.GetViewMatrix();
    glm::vec3 camPos = m_mainCamera.GetPosition();

    for (auto& pair : Resources::Get().Shaders().All()) {
        auto& shader = pair.second;
        glUseProgram(shader);
        auto uniAlphaMask = glGetUniformLocation(shader, "alphaMask");
		if (uniAlphaMask != -1) {
            glUniform1i(uniAlphaMask, 2);
        }
       
        GLint uniDiffuse = glGetUniformLocation(shader, "diffuse");
        if (uniDiffuse != -1)
            glUniform1i(uniDiffuse, 0);
        
        GLint uniNormal = glGetUniformLocation(shader, "normalMap");
        if (uniNormal != -1)
            glUniform1i(uniNormal, 3);

        glm::mat4 projection = m_mainCamera.GetProjectionMatrix();
        GLint uniProjection = glGetUniformLocation(shader, "proj");
        if (uniProjection != -1)
            glUniformMatrix4fv(uniProjection, 1, GL_FALSE,
                               glm::value_ptr(projection));

        GLint uniView = glGetUniformLocation(shader, "view");
        if (uniView != -1)
            glUniformMatrix4fv(uniView, 1, GL_FALSE, glm::value_ptr(view));

        GLint viewPosLoc = glGetUniformLocation(shader, "viewPos");
        if (viewPosLoc != -1)
            glUniform3f(viewPosLoc, camPos.x, camPos.y, camPos.z);

        for (const auto& scene : m_renderer.GetScenes()) {
            int i = 0;
            GLint uniDepthMap = glGetUniformLocation(shader, "shadowMap");
            if (uniDepthMap != -1) {
                auto uniDiffuse = glGetUniformLocation(shader, "diffuse");
                glUniform1i(uniDiffuse, 0);
                glUniform1i(uniDepthMap, 1);
               
                auto depthMap = scene->GetDepthMap();
                if (depthMap != 0) {
                    glActiveTexture(GL_TEXTURE1);
                    glBindTexture(GL_TEXTURE_2D, depthMap);
                    glActiveTexture(GL_TEXTURE0);
                    shader.Validate();
                }
            }

            for (auto& light : scene->GetLights()) {
                switch (light.GetType()) {
                case Light::Type::Directional: {
                    if (IsKeyPressed(Key::KEY_PAGEUP))
                        light.GetTransform().Translate(glm::vec3(0, 0.01, 0));
                    if (IsKeyPressed(Key::KEY_PAGEDOWN))
                        light.GetTransform().Translate(glm::vec3(0, -0.01, 0));

                    const auto& direction = light.GetTransform().GetPosition();

                    auto color = light.GetColor();
                    glUniform3f(
                        glGetUniformLocation(shader, "dirLight.position"),
                        direction.x, direction.y, direction.z);
                    glUniform3f(
                        glGetUniformLocation(shader, "dirLight.ambient"),
                        color.x * 0.05f, color.y * 0.05f, color.z * 0.05f);
                    glUniform3f(
                        glGetUniformLocation(shader, "dirLight.diffuse"),
                        color.x, color.y, color.z);
                    glUniform3f(
                        glGetUniformLocation(shader, "dirLight.specular"),
                        color.x, color.y, color.z);
                    GLint lightSpace =
                        glGetUniformLocation(shader, "lightSpaceMatrix");
                    if (lightSpace != -1) {
                        glUniformMatrix4fv(
                            lightSpace, 1, false,
                            glm::value_ptr(light.GetSpaceMatrix()));
                    }
                } break;
                case Light::Type::Point: {
                    auto& transform = light.GetTransform();
                    // transform.Translate(glm::vec3(1, 0, 0));
                    transform.Rotate(glm::vec3(0, 1, 0), deltaTime * .1f);
                    // transform.Translate(glm::vec3(-1, 0, 0));
                    auto pos =
                        transform.GetWorldMatrix() * glm::vec4(0, 0, 0, 1);
                    auto number = std::to_string(i);
                    auto color = light.GetColor();
                    glUniform3f(
                        glGetUniformLocation(
                            shader,
                            ("pointLights[" + number + "].position").c_str()),
                        pos.x, pos.y, pos.z);
                    glUniform3f(
                        glGetUniformLocation(
                            shader,
                            ("pointLights[" + number + "].ambient").c_str()),
                        color.x * 0.05f, color.y * 0.05f, color.z * 0.05f);
                    glUniform3f(
                        glGetUniformLocation(
                            shader,
                            ("pointLights[" + number + "].diffuse").c_str()),
                        color.x, color.y, color.z);
                    glUniform3f(
                        glGetUniformLocation(
                            shader,
                            ("pointLights[" + number + "].specular").c_str()),
                        color.x, color.y, color.z);
                    glUniform1f(
                        glGetUniformLocation(
                            shader,
                            ("pointLights[" + number + "].constant").c_str()),
                        1.0f);
                    glUniform1f(
                        glGetUniformLocation(
                            shader,
                            ("pointLights[" + number + "].linear").c_str()),
                        0.09f);
                    glUniform1f(
                        glGetUniformLocation(
                            shader,
                            ("pointLights[" + number + "].quadratic").c_str()),
                        0.032f);
                    i++;
                } break;
                case Light::Type::Spot:
                    break;
                default:
                    break;
                }
            }
        }
    }
    glUseProgram(0);
    m_renderer.Render();
}

void Engine::DisplayTerminated() { m_renderer.TermDisplay(); }

void Engine::Input(int8_t x, int8_t y) {
    float sensitivity = 0.1f;
    m_mainCamera.Rotate(x * sensitivity, -y * sensitivity);
}

void Engine::KeyPressed(Key key) { m_pressedKeys.insert(key); }

void Engine::KeyReleased(Key key) {
    auto iter = std::find(m_pressedKeys.begin(), m_pressedKeys.end(), key);
    if (iter != m_pressedKeys.end())
        m_pressedKeys.erase(iter);
}

bool Engine::IsKeyPressed(Key key) {
    auto iter = std::find(m_pressedKeys.begin(), m_pressedKeys.end(), key);
    return iter != m_pressedKeys.end();
}

void Engine::Resize(int w, int h) {
    m_renderer.Resize(w, h);
    m_width = w;
    m_height = h;
    if (m_height > 0) // Division by zero
        m_mainCamera.SetProjectionPerspective(60.f, (float)m_width / m_height,
                                              0.1f, 100.f);
}

Audio& Engine::getAudio() { return m_audio; }

Renderer& Engine::GetRenderer() { return m_renderer; }
