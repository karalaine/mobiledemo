#define STB_TRUETYPE_IMPLEMENTATION
#include "TextNode.h"
#include "Resources.h"
#include "stb_truetype.h"
#include <memory>
#include <vector>

#include "Log.h"
namespace {
constexpr std::size_t MAX_TEXT_LENGTH = 256;
constexpr char first = ' ';
constexpr char last = '~';
constexpr int charCount = last - first;
struct TextVertex {
    glm::vec2 position;
    glm::vec2 texCoord;
};
} // namespace
TextNode::TextNode(Shader& textShader)
    : Node(1), m_shader(textShader), m_text("EMPTY"), m_indexElementCount(0),
      m_fontTextureId(), m_charData(charCount) {}

void TextNode::InitNode() {
    m_shader();
    glActiveTexture(GL_TEXTURE0);
    auto texLoc = glGetUniformLocation(m_shader, "textAtlas");
    glUniform1i(texLoc, 0);

    auto& assetMgr = Resources::Get().Assets();
    auto& font = assetMgr.Open("moto.ttf", std::ifstream::binary);

    auto atlasData = std::make_unique<uint8_t[]>(1024 * 1024);

    stbtt_pack_context context;
    if (!stbtt_PackBegin(&context, atlasData.get(), 1024, 1024, 0, 1, nullptr))
        LOGW("Failed to initialize font");

    stbtt_PackSetOversampling(&context, 2, 2);
    auto fontData = font.GetBuffer();
    auto fontDataPtr = reinterpret_cast<unsigned char*>(fontData.get());
    if (!stbtt_PackFontRange(&context, fontDataPtr, 0, 80, first, charCount,
                             m_charData.data())) {
        LOGW("Failed to pack font");
    }

    stbtt_PackEnd(&context);
    glGenTextures(1, &m_fontTextureId);

    glBindTexture(GL_TEXTURE_2D, m_fontTextureId);
    // glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, 1024, 1024, 0, GL_RED,
                 GL_UNSIGNED_BYTE, atlasData.get());
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // glGenerateMipmap(GL_TEXTURE_2D);

    glGenVertexArrays(1, &m_vaos[0]);
    glBindVertexArray(m_vaos[0]);
    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(TextVertex) * 4 * MAX_TEXT_LENGTH,
                 nullptr, GL_DYNAMIC_DRAW);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, nullptr);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4,
                          reinterpret_cast<GLvoid*>(sizeof(float) * 2));
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &m_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * 6 * MAX_TEXT_LENGTH,
                 nullptr, GL_DYNAMIC_DRAW);
}
void TextNode::SetText(const std::string& newText) {
    if (newText == m_text) {
        return;
    }
	m_text = newText;
    std::vector<TextVertex> vertices;
    std::vector<glm::vec2> uvs;
    std::vector<GLuint> indexes;

    float offsetX = 0, offsetY = 0;
    uint16_t lastIndex = 0;
    for (auto c : m_text) {

        stbtt_aligned_quad quad;
        stbtt_GetPackedQuad(m_charData.data(), 1024, 1024, c - first, &offsetX,
                            &offsetY, &quad, 1);
        auto xmin = quad.x0;
        auto xmax = quad.x1;
        auto ymin = -quad.y1;
        auto ymax = -quad.y0;

        vertices.emplace_back(
            TextVertex{glm::vec2(xmin, ymin), glm::vec2(quad.s0, quad.t1)});
        vertices.emplace_back(
            TextVertex{glm::vec2(xmin, ymax), glm::vec2(quad.s0, quad.t0)});
        vertices.emplace_back(
            TextVertex{glm::vec2(xmax, ymax), glm::vec2(quad.s1, quad.t0)});
        vertices.emplace_back(
            TextVertex{glm::vec2(xmax, ymin), glm::vec2(quad.s1, quad.t1)});
        indexes.push_back(lastIndex);
        indexes.push_back(lastIndex + 2);
        indexes.push_back(lastIndex + 1);
        indexes.push_back(lastIndex);
        indexes.push_back(lastIndex + 3);
        indexes.push_back(lastIndex + 2);

        lastIndex += 4;
    }

    m_indexElementCount = indexes.size();

    glBindVertexArray(m_vaos[0]);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(TextVertex) * vertices.size(),
                    vertices.data());

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(GLuint) * indexes.size(),
                    indexes.data());
}
void TextNode::RenderOpaque() {
    m_shader();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_fontTextureId);
    glBindVertexArray(m_vaos[0]);
    glDrawElements(GL_TRIANGLES, m_indexElementCount, GL_UNSIGNED_INT, nullptr);
}
