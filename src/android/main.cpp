
/**
* This is the main entry point of a native application that is using
* android_native_app_glue.  It runs in its own thread, with its own
* event loop for receiving input events and doing other things.
*/
#include "android_native_app_glue.h"

#include "Engine.h"
/**
* Process the next input event.
*/
static int32_t HandleInput(android_app* app, AInputEvent* event) {
	Engine* engine = (Engine*)app->userData;
	if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_KEY)
	{
		if (AKeyEvent_getKeyCode(event) == 4 && 
			AKeyEvent_getAction(event) == AKEY_EVENT_ACTION_UP)
		{
			app->destroyRequested = 1;
			return 1;
		}
	}
	return 0;
}

/**
* Process the next main command.
*/
static void HandleCmd(android_app* app, int32_t cmd) {
	Engine* engine = (Engine*)app->userData;
	switch (cmd) {
	case APP_CMD_SAVE_STATE:
		// The system has asked us to save our current state.  Do so.
		app->savedState = new saved_state();
		*((struct saved_state*)app->savedState) = engine->state;
		app->savedStateSize = sizeof(saved_state);
		break;
	case APP_CMD_INIT_WINDOW:
		// The window is being shown, get it ready.
        engine->animating = 1;
        if(app->activity != nullptr) {
            Engine::androidActivity = app->activity;
        }
		if (app->window != NULL) {
			auto nativeWindow = app->window;
			engine->Initialize(nativeWindow);
			engine->Update();
		}

		break;
	case APP_CMD_TERM_WINDOW:
		// The window is being hidden or closed, clean it up.
		engine->DisplayTerminated();
		break;
	case APP_CMD_GAINED_FOCUS:
		// When our app gains focus, we start monitoring the accelerometer.
		break;
	case APP_CMD_LOST_FOCUS:
		// Also stop animating.
		engine->getAudio().Pause();
		engine->animating = 0;
		engine->Update();
		break;
	}
}

void android_main(android_app* state) {
	Engine engine;

	state->userData = &engine;
	state->onAppCmd = HandleCmd;
	state->onInputEvent = HandleInput;
	//engine.app = state;

	if (state->savedState != NULL) {
		// We are starting with a previous saved state; restore from it.
		engine.state = *(struct saved_state*)state->savedState;
	}


	// loop waiting for stuff to do.

	while (1) {
		// Read all pending events.
		int ident;
		int events;
		struct android_poll_source* source;

		// If not animating, we will block forever waiting for events.
		// If animating, we loop until all events are read, then continue
		// to draw the next frame of animation.
		while ((ident = ALooper_pollAll(engine.animating ? 0 : -1, NULL, &events,
			(void**)&source)) >= 0) {

			// Process this event.
			if (source != NULL) {
				source->process(state, source);
			}

			// Check if we are exiting.
			if (state->destroyRequested != 0) {
				engine.DisplayTerminated();
				return;
			}
		}

		if (engine.animating) {
			// Done with events; draw next animation frame.
			// Drawing is throttled to the screen update rate, so there
			// is no need to do timing here.
			engine.Update();
		}
	}
}
