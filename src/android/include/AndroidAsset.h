#pragma once
#include <memory>
#include "Asset.h"

struct AAsset;

class AndroidAsset : public Asset
{
public:
	explicit AndroidAsset(AAsset* asset);
	AndroidAsset(AndroidAsset&& other);
	AndroidAsset& operator=(AndroidAsset&& other);

	~AndroidAsset();
	std::unique_ptr<char[]> GetBuffer() override;
	long GetLength() override;
	int GetFileDescpritor() override;

	// Inherited via Asset
	virtual size_t Read(char * buf, size_t pCount) override;
	virtual int Seek(size_t pOffset, std::ios_base::seekdir) override;
	virtual size_t Tell() override;

private:
	AndroidAsset(const AndroidAsset&) = delete;
	AndroidAsset& operator=(const AndroidAsset&) = delete;

	AAsset* m_filePtr;
	long m_length;
	long m_start;
	int m_fileDescpritor;
	// Inherited via Asset
	virtual bool IsValid() override;

};
