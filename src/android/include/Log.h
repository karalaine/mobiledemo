#pragma once
#include <android/log.h>
#include <sstream>

inline void LOGI(const char* format)
{
    __android_log_write(ANDROID_LOG_INFO, "DemoEngine", format);
}

inline void LOGW(const char* format)
{
    __android_log_write(ANDROID_LOG_WARN, "DemoEngine", format);
}

template<typename T, typename... Targs>
inline void LOGI(const char* format, T value, Targs... Fargs) // recursive variadic function
{
    for (; *format != '\0'; format++) {
        if (*format == '%') {
            std::stringstream ss;
            ss << value;
            __android_log_write(ANDROID_LOG_INFO, "DemoEngine", ss.str().c_str());
            LOGI(format + 1, Fargs...); // recursive call
            return;
        }
        __android_log_write(ANDROID_LOG_INFO, "DemoEngine", format);
    }
}

template<typename T, typename... Targs>
inline void LOGW(const char* format, T value, Targs... Fargs)
{

    for (; *format != '\0'; format++) {
        if (*format == '%') {
            std::stringstream ss;
            ss << value;
            __android_log_write(ANDROID_LOG_WARN, "DemoEngine",  ss.str().c_str());
            LOGW(format + 1, Fargs...); // recursive call
            return;
        }
        __android_log_write(ANDROID_LOG_WARN, "DemoEngine", format);
    }
}