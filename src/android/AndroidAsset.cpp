#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <vector>

#include "AndroidAsset.h"

AndroidAsset::AndroidAsset(AAsset * asset) :
	m_filePtr(asset),
	m_fileDescpritor(AAsset_openFileDescriptor(m_filePtr, &m_start, &m_length))
{
	//Compressed file must fetch length seperately
	if (m_fileDescpritor == -1)
		m_length = AAsset_getLength(m_filePtr);
}

AndroidAsset::AndroidAsset(AndroidAsset && other)
{
	m_filePtr = other.m_filePtr;
	m_fileDescpritor = other.m_fileDescpritor;
	m_length = other.m_length;
	m_start = other.m_start;
	other.m_filePtr = nullptr;
}

AndroidAsset & AndroidAsset::operator=(AndroidAsset && other)
{
	m_filePtr = other.m_filePtr;
	m_fileDescpritor = other.m_fileDescpritor;
	m_length = other.m_length;
	m_start = other.m_start;
	other.m_filePtr = nullptr;
	return *this;
}


AndroidAsset::~AndroidAsset()
{
	if(m_filePtr != nullptr)
		AAsset_close(m_filePtr);
}

std::unique_ptr<char[]> AndroidAsset::GetBuffer()
{
	auto voidData = AAsset_getBuffer(m_filePtr);
	auto charData = static_cast<const char *>(voidData);
	auto tmp = std::make_unique<char[]>(m_length + 1);
	std::copy(charData, charData + m_length, tmp.get());
	tmp.get()[m_length] = '\0';
	return tmp;
}

long AndroidAsset::GetLength()
{
	return m_length;
}

int AndroidAsset::GetFileDescpritor()
{
	return m_fileDescpritor;
}

bool AndroidAsset::IsValid()
{
	return m_filePtr != nullptr;
}

size_t AndroidAsset::Read(char * buf, size_t pCount)
{
	int read = AAsset_read(m_filePtr, buf, pCount);
	if (read <= 0)
	{
		//Error or end of file
		return 0;
	}
	return static_cast<size_t>(read);
}

int AndroidAsset::Seek(size_t pOffset, std::ios_base::seekdir dir)
{
	int seekMode;
	switch (dir)
	{
	case std::ios_base::beg:
		seekMode = SEEK_SET;
		break;
	case std::ios_base::cur:
		seekMode = SEEK_CUR;
		break;
	case std::ios_base::end:
		seekMode = SEEK_END;
		break;
	}
	return AAsset_seek(m_filePtr, pOffset, seekMode);
}

size_t AndroidAsset::Tell()
{
	return AAsset_getRemainingLength(m_filePtr) - m_length;
}
