#include <assert.h>
#include <jni.h>
#include <string.h>

// for __android_log_print(ANDROID_LOG_INFO, "YourApp", "formatted message");
// #include <android/log.h>



// for native asset manager
#include <sys/types.h>

#include <functional>

#include "AndroidAudio.h"
#include "Asset.h"


class Audio::Impl
{
public:
~Impl()
{
	// destroy file descriptor audio player object, and invalidate all associated interfaces
	if (fdPlayerObject != NULL) {
		(*fdPlayerObject)->Destroy(fdPlayerObject);
		fdPlayerObject = NULL;
		fdPlayerPlay = NULL;
		fdPlayerSeek = NULL;
	}
	// destroy output mix object, and invalidate all associated interfaces
	if (outputMixObject != NULL) {
		(*outputMixObject)->Destroy(outputMixObject);
		outputMixObject = NULL;
	}

	// destroy engine object, and invalidate all associated interfaces
	if (engineObject != NULL) {
		(*engineObject)->Destroy(engineObject);
		engineObject = NULL;
		engineEngine = NULL;
	}

}

void CreateEngine()
{
	// create engine
	SLresult result = slCreateEngine(&engineObject, 0, NULL, 0, NULL, NULL);
	assert(SL_RESULT_SUCCESS == result);

	// realize the engine
	result = (*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE);
	assert(SL_RESULT_SUCCESS == result);
	(void)result;

	// get the engine interface, which is needed in order to create other objects
	result = (*engineObject)->GetInterface(engineObject, SL_IID_ENGINE, &engineEngine);
	assert(SL_RESULT_SUCCESS == result);
	(void)result;

	const SLInterfaceID ids[] = { SL_IID_VOLUME };
	const SLboolean req[] = { SL_BOOLEAN_FALSE };
	result = (*engineEngine)->CreateOutputMix(engineEngine,
		&(outputMixObject), 1, ids, req);
	assert(SL_RESULT_SUCCESS == result);
	result = (*outputMixObject)->Realize(outputMixObject,
		SL_BOOLEAN_FALSE);
	assert(SL_RESULT_SUCCESS == result);

}

bool Open(const std::string & fileName, AssetManager & mgr)
{
	SLresult result;

	auto& asset = mgr.Open(fileName);

	// open asset as file descriptor
	off_t start = 0, length;
	int fd = asset.GetFileDescpritor();
	length = asset.GetLength();
	assert(0 <= fd);
	mgr.Close(fileName);

	// configure audio source
	SLDataLocator_AndroidFD loc_fd = { SL_DATALOCATOR_ANDROIDFD, fd, start, length };
	SLDataFormat_MIME format_mime = { SL_DATAFORMAT_MIME, NULL, SL_CONTAINERTYPE_UNSPECIFIED };
	SLDataSource audioSrc = { &loc_fd, &format_mime };

	// configure audio sink
	SLDataLocator_OutputMix loc_outmix = { SL_DATALOCATOR_OUTPUTMIX, outputMixObject };
	SLDataSink audioSnk = { &loc_outmix, NULL };

	// create audio player
	const SLInterfaceID ids[3] = { SL_IID_SEEK, SL_IID_MUTESOLO, SL_IID_VOLUME };
	const SLboolean req[3] = { SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE };
	result = (*engineEngine)->CreateAudioPlayer(engineEngine, &fdPlayerObject, &audioSrc, &audioSnk,
		3, ids, req);
	assert(SL_RESULT_SUCCESS == result);
	(void)result;

	// realize the player
	result = (*fdPlayerObject)->Realize(fdPlayerObject, SL_BOOLEAN_FALSE);
	assert(SL_RESULT_SUCCESS == result);
	(void)result;

	// get the play interface
	result = (*fdPlayerObject)->GetInterface(fdPlayerObject, SL_IID_PLAY, &fdPlayerPlay);
	assert(SL_RESULT_SUCCESS == result);
	(void)result;

	// get the seek interface
	result = (*fdPlayerObject)->GetInterface(fdPlayerObject, SL_IID_SEEK, &fdPlayerSeek);
	assert(SL_RESULT_SUCCESS == result);
	(void)result;

	result = (*fdPlayerPlay)->SetPlayState(fdPlayerPlay, SL_PLAYSTATE_PLAYING);
	assert(SL_RESULT_SUCCESS == result);
	(void)result;

	return JNI_TRUE;
}

void Pause()
{
	if (fdPlayerPlay != NULL) {
		SLresult result = (*fdPlayerPlay)->SetPlayState(fdPlayerPlay, SL_PLAYSTATE_PAUSED);
		assert(SL_RESULT_SUCCESS == result);
		(void)result;
	}
}

void Play()
{
	if (fdPlayerPlay != NULL) {
		SLresult result = (*fdPlayerPlay)->SetPlayState(fdPlayerPlay, SL_PLAYSTATE_PLAYING);
		assert(SL_RESULT_SUCCESS == result);
		(void)result;
	}
}

int GetPos()
{
	if (fdPlayerPlay != NULL) {
		SLmillisecond millis = 0;
		if ((*fdPlayerPlay)->GetPosition(fdPlayerPlay, &millis) == SL_RESULT_SUCCESS)
			return millis;
	}
	return -1;
}

void SetPos(int pos)
{
	if (fdPlayerSeek)
	{
		(*fdPlayerSeek)->SetPosition(fdPlayerSeek, pos, SL_SEEKMODE_FAST);
	}
}

bool IsPlaying()
{
	if (fdPlayerPlay != NULL) {
		SLuint32 state;
		if((*fdPlayerPlay)->GetPlayState(fdPlayerPlay, &state) == SL_RESULT_SUCCESS)
			return state == SL_PLAYSTATE_PLAYING;
	}
	return false;
}

private:
	SLObjectItf engineObject = NULL;
	SLEngineItf engineEngine;
	SLObjectItf outputMixObject = NULL;
	SLObjectItf fdPlayerObject = NULL;
	SLPlayItf fdPlayerPlay;
	SLSeekItf fdPlayerSeek;

};

Audio::Audio() :
	m_impl(new Impl())
{}

Audio::~Audio()
{
}

void Audio::CreateEngine()
{
	m_impl->CreateEngine();
}

bool Audio::Open(const std::string & fileName, AssetManager & mgr)
{
	return m_impl->Open(fileName, mgr);
}

void Audio::Pause()
{
	m_impl->Pause();
}

void Audio::Play()
{
	m_impl->Play();
}

int Audio::GetPositionInMillis()
{
	return m_impl->GetPos();
}

void Audio::SetPositionInMillis(int pos)
{
	m_impl->SetPos(pos);
}

bool Audio::IsPlaying()
{
	return m_impl->IsPlaying();
}
