#include <map>

#include <android/asset_manager.h>

#include "AssetManager.h"
#include "AndroidAsset.h"
#include "android_native_app_glue.h"
#include "Engine.h"

class AssetManager::Impl
{
public:

	Impl()
	{
		m_nativeManager = Engine::androidActivity->assetManager;
	}

	Asset& Open(const std::string & filename, std::ios_base::openmode)
	{
		auto pos = m_assets.find(filename);
		if (pos == m_assets.end())
		{
			auto nativePtr = AAssetManager_open(m_nativeManager, filename.c_str(), AASSET_MODE_UNKNOWN);
			if (nativePtr != nullptr)
			{
				auto pair = m_assets.emplace(std::string(filename),AndroidAsset(nativePtr));
				return pair.first->second;

			}
			throw std::runtime_error("File not found! " + filename);
		}
		else
		{
			return pos->second;
		}
	}

	void Close(const std::string & filename)
	{
		m_assets.erase(filename);

	}
private:
	AAssetManager* m_nativeManager;

	std::map<std::string, AndroidAsset> m_assets;
};

AssetManager::AssetManager() :
	_Impl(new Impl())
{}

AssetManager::~AssetManager()
{
}


Asset& AssetManager::Open(const std::string & filename, std::ios_base::openmode mode)
{
	return _Impl->Open(filename, mode);
}

void AssetManager::Close(const std::string & filename)
{
	_Impl->Close(filename);
}

char AssetManager::GetOSSeparator()
{
	return '/';
}
