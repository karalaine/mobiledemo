#include "Material.h"
#include "Resources.h"

Material::Material(const aiMaterial* imported)
    : m_ambient(), m_diffuse(), m_specular() {
 
    aiColor3D temp{0, 0, 0};
	if (imported != nullptr) {	
		if (imported->Get(AI_MATKEY_COLOR_DIFFUSE, temp) == AI_SUCCESS) {
			m_diffuse = glm::vec3(temp.r, temp.g, temp.b);
		}
		if (imported->Get(AI_MATKEY_COLOR_SPECULAR, temp) == AI_SUCCESS) {
			m_specular = glm::vec3(temp.r, temp.g, temp.b);
		}
		if (imported->Get(AI_MATKEY_COLOR_AMBIENT, temp) == AI_SUCCESS) {
			m_ambient = glm::vec3(temp.r, temp.g, temp.b);
		}
	}
}

unsigned int Material::GetShaderProgram() {
    auto& res = Resources::Get();
    auto shader = res.Shaders().Get(m_shaderName);
    return (*shader);
}

void Material::LoadAllTextures() {
    for (auto& texturePath : m_texturePaths) {
        auto texture = Resources::Get().Textures().Get(texturePath.second);
        if (texture) {
            texture->Load();
        } else {
            LOGW("No texture found when material did have %"
                 " path set!", texturePath.second);
        }
	}
}