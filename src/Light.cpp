#include <algorithm> 
#include <glm/gtc/matrix_transform.hpp>
#include "Light.h"

Light::Light()
 : m_transform(),
 m_color(1)
{
}

Light::Light(Type type)
	: m_type(type),
	m_color(1)
{

}

Transform & Light::GetTransform()
{
	return m_transform;
}

glm::vec3 Light::GetColor()
{
	return m_color;
}

glm::mat4 Light::GetSpaceMatrix()
{
	float near_plane = 1.0f, far_plane = 15.f;
	glm::mat4 lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
	glm::mat4 lightView = glm::lookAt(m_transform.GetPosition(),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f));

	return lightProjection * lightView;
}

Light::Type Light::GetType()
{
	return m_type;
}

void Light::SetColor(glm::vec3 color)
{
	m_color = color;
}
	
void Light::SetColor(unsigned char r, unsigned char g, unsigned char b)
{
	m_color.r = std::min(r / 255.0f, 1.0f);
	m_color.b = std::min(g / 255.0f, 1.0f);
	m_color.g = std::min(b / 255.0f, 1.0f);
}
