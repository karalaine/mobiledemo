//
// Copyright (c) 2014 The ANGLE Project Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//

// Win32Window.cpp: Implementation of OSWindow for Win32 (Windows)

#include "Win32Window.h"

#include <direct.h>
#include <iostream>
#include <sstream>

Key VirtualKeyCodeToKey(WPARAM key) {
    switch (key) {
    case VK_ESCAPE:
        return Key::KEY_ESCAPE;
    case 'A':
        return Key::KEY_A;
    case 'D':
        return Key::KEY_D;
    case 'S':
        return Key::KEY_S;
    case 'W':
        return Key::KEY_W;
    case VK_PRIOR:
        return Key::KEY_PAGEUP;
    case VK_NEXT:
        return Key::KEY_PAGEDOWN;
    }

    return Key(0);
}

LRESULT CALLBACK Win32Window::WndProc(HWND hWnd, UINT message, WPARAM wParam,
                                      LPARAM lParam) {
    switch (message) {
    case WM_NCCREATE: {
        LPCREATESTRUCT pCreateStruct = reinterpret_cast<LPCREATESTRUCT>(lParam);
        SetWindowLongPtr(
            hWnd, GWLP_USERDATA,
            reinterpret_cast<LONG_PTR>(pCreateStruct->lpCreateParams));
        return DefWindowProcA(hWnd, message, wParam, lParam);
    }
    }

    Win32Window* window =
        reinterpret_cast<Win32Window*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
    if (window) {
        switch (message) {
        case WM_CREATE: {
            LPCREATESTRUCT params = reinterpret_cast<LPCREATESTRUCT>(lParam);
            if (params->hwndParent) // Ignore console window
            {
                Event event;
                event.Type = Event::EVENT_CREATED;
                window->pushEvent(event);
            }
            break;
        }
        case WM_DESTROY:
        case WM_CLOSE: {
            Event event;
            event.Type = Event::EVENT_CLOSED;
            window->pushEvent(event);
            break;
        }

        case WM_MOVE: {
            RECT winRect;
            GetClientRect(hWnd, &winRect);

            POINT topLeft;
            topLeft.x = winRect.left;
            topLeft.y = winRect.top;
            ClientToScreen(hWnd, &topLeft);

            Event event;
            event.Type = Event::EVENT_MOVED;
            event.Move.X = topLeft.x;
            event.Move.Y = topLeft.y;
            window->pushEvent(event);

            break;
        }

        case WM_SIZE: {
            RECT winRect;
            GetClientRect(hWnd, &winRect);

            POINT topLeft;
            topLeft.x = winRect.left;
            topLeft.y = winRect.top;
            ClientToScreen(hWnd, &topLeft);

            POINT botRight;
            botRight.x = winRect.right;
            botRight.y = winRect.bottom;
            ClientToScreen(hWnd, &botRight);

            Event event;
            event.Type = Event::EVENT_RESIZED;
            event.Size.Width = botRight.x - topLeft.x;
            event.Size.Height = botRight.y - topLeft.y;
            window->pushEvent(event);

            break;
        }

        case WM_SETFOCUS: {
            Event event;
            event.Type = Event::EVENT_GAINED_FOCUS;
            window->pushEvent(event);
            break;
        }

        case WM_KILLFOCUS: {
            Event event;
            event.Type = Event::EVENT_LOST_FOCUS;
            window->pushEvent(event);
            break;
        }

        case WM_KEYDOWN:
        case WM_SYSKEYDOWN:
        case WM_KEYUP:
        case WM_SYSKEYUP: {
            bool down = (message == WM_KEYDOWN || message == WM_SYSKEYDOWN);

            Event event;
            event.Type =
                down ? Event::EVENT_KEY_PRESSED : Event::EVENT_KEY_RELEASED;
            event.Key.Alt = HIWORD(GetAsyncKeyState(VK_MENU)) != 0;
            event.Key.Control = HIWORD(GetAsyncKeyState(VK_CONTROL)) != 0;
            event.Key.Shift = HIWORD(GetAsyncKeyState(VK_SHIFT)) != 0;
            event.Key.System = HIWORD(GetAsyncKeyState(VK_LWIN)) ||
                               HIWORD(GetAsyncKeyState(VK_RWIN));
            event.Key.Code = VirtualKeyCodeToKey(wParam);
            window->pushEvent(event);

            break;
        }
        case WM_MOUSEMOVE: {
            /*	if (!window->mIsMouseInWindow)
                                {
                                        window->mIsMouseInWindow = true;
                                }

                                int mouseX =
               static_cast<uint8_t>(LOWORD(lParam)); int mouseY =
               static_cast<uint8_t>(HIWORD(lParam));

                                Event event;
                                event.Type = Event::EVENT_MOUSE_MOVED;
                                event.MouseMove.X = mouseX;
                                event.MouseMove.Y = mouseY;
                                window->pushEvent(event);
                                RECT rc;
                                GetClientRect(hWnd, &rc);
                                POINT pt;
                                ScreenToClient(hWnd, &pt);

                                if (pt.x >= rc.right)
                                {
                                        pt.x = rc.right - 1;
                                }
                                else
                                {
                                        if (pt.x < rc.left)
                                        {
                                                pt.x = rc.left;
                                        }
                                }

                                if (pt.y >= rc.bottom)
                                        pt.y = rc.bottom - 1;
                                else
                                        if (pt.y < rc.top)
                                                pt.y = rc.top;

                                // Convert client coordinates to screen
               coordinates.

                                ClientToScreen(hWnd, &pt);
                                SetCursorPos(pt.x, pt.y);
                                break;*/
        }
        case WM_INPUT: {
            UINT dwSize;

            GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &dwSize,
                            sizeof(RAWINPUTHEADER));
            LPBYTE lpb = new BYTE[dwSize];
            if (lpb == NULL) {
                return 0;
            }

            if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, lpb, &dwSize,
                                sizeof(RAWINPUTHEADER)) != dwSize)
                OutputDebugString(
                    TEXT("GetRawInputData does not return correct size !\n"));

            RAWINPUT* raw = (RAWINPUT*)lpb;
            if (raw->header.dwType == RIM_TYPEMOUSE) {
                Event event;
                event.Type = Event::EVENT_MOUSE_MOVED;
                event.MouseMove.X = static_cast<int8_t>(raw->data.mouse.lLastX);
                event.MouseMove.Y = static_cast<int8_t>(raw->data.mouse.lLastY);
                window->pushEvent(event);
                RECT rc;
                GetClientRect(hWnd, &rc);
                POINT pt;
                pt.x = static_cast<LONG>((rc.right - rc.left) * 0.5);
                pt.y = static_cast<LONG>((rc.bottom - rc.top) * 0.5);
                ClientToScreen(hWnd, &pt);
                SetCursorPos(pt.x, pt.y);
            }

            delete[] lpb;
        }
        }
    }
    return DefWindowProcA(hWnd, message, wParam, lParam);
}

Win32Window::Win32Window()
    : mNativeWindow(0), mParentWindow(0), mNativeDisplay(0) {}

Win32Window::~Win32Window() { destroy(); }

bool Win32Window::initialize(const std::string& name, size_t width,
                             size_t height) {
    destroy();
    ShowCursor(false);
    // Use a new window class name for ever window to ensure that a new window
    // can be created even if the last one was not properly destroyed
    static size_t windowIdx = 0;
    std::ostringstream nameStream;
    nameStream << name << "_" << windowIdx++;

    mParentClassName = nameStream.str();
    mChildClassName = mParentClassName + "_Child";

    // Work around compile error from not defining "UNICODE" while Chromium does
    const LPSTR idcArrow = MAKEINTRESOURCEA(32512);

    WNDCLASSEXA parentWindowClass {};

    parentWindowClass.cbSize = sizeof(WNDCLASSEXA);
    parentWindowClass.style = 0;
    parentWindowClass.lpfnWndProc = WndProc;
    parentWindowClass.cbClsExtra = 0;
    parentWindowClass.cbWndExtra = 0;
    parentWindowClass.hInstance = GetModuleHandle(NULL);
    parentWindowClass.hIcon = NULL;
    parentWindowClass.hCursor = LoadCursorA(NULL, idcArrow);
    parentWindowClass.hbrBackground = 0;
    parentWindowClass.lpszMenuName = NULL;
    parentWindowClass.lpszClassName = mParentClassName.c_str();
    if (!RegisterClassExA(&parentWindowClass)) {
        return false;
    }

    WNDCLASSEXA childWindowClass {};
    childWindowClass.cbSize = sizeof(WNDCLASSEXA);
    childWindowClass.style = CS_OWNDC;
    childWindowClass.lpfnWndProc = WndProc;
    childWindowClass.cbClsExtra = 0;
    childWindowClass.cbWndExtra = 0;
    childWindowClass.hInstance = GetModuleHandle(NULL);
    childWindowClass.hIcon = NULL;
    childWindowClass.hCursor = LoadCursorA(NULL, idcArrow);
    childWindowClass.hbrBackground = 0;
    childWindowClass.lpszMenuName = NULL;
    childWindowClass.lpszClassName = mChildClassName.c_str();
    if (!RegisterClassExA(&childWindowClass)) {
        return false;
    }

    DWORD parentStyle = WS_CAPTION | WS_THICKFRAME | WS_MINIMIZEBOX |
                        WS_MAXIMIZEBOX | WS_SYSMENU;
    DWORD parentExtendedStyle = WS_EX_APPWINDOW;

    RECT sizeRect = {0, 0, static_cast<LONG>(width), static_cast<LONG>(height)};
    AdjustWindowRectEx(&sizeRect, parentStyle, FALSE, parentExtendedStyle);

    mParentWindow = CreateWindowExA(
        parentExtendedStyle, mParentClassName.c_str(), name.c_str(),
        parentStyle, CW_USEDEFAULT, CW_USEDEFAULT,
        sizeRect.right - sizeRect.left, sizeRect.bottom - sizeRect.top, NULL,
        NULL, GetModuleHandle(NULL), this);

    mNativeWindow =
        CreateWindowExA(0, mChildClassName.c_str(), name.c_str(), WS_CHILD, 0,
                        0, static_cast<int>(width), static_cast<int>(height),
                        mParentWindow, NULL, GetModuleHandle(NULL), this);

    mNativeDisplay = GetDC(mNativeWindow);
    if (!mNativeDisplay) {
        destroy();
        return false;
    }
    SetCapture(mNativeWindow);
    RAWINPUTDEVICE Rid[1];
    Rid[0].usUsagePage = 0x01;
    Rid[0].usUsage = 0x02;
    Rid[0].dwFlags =
        RIDEV_NOLEGACY; // adds HID mouse and also ignores legacy mouse messages
    Rid[0].hwndTarget = 0;
    if (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) == FALSE) {
        // registration failed. Call GetLastError for the cause of the error
        // auto err = GetLastError();
    }
    return true;
}

void Win32Window::destroy() {
    if (mNativeDisplay) {
        ReleaseDC(mNativeWindow, mNativeDisplay);
        mNativeDisplay = 0;
    }

    if (mNativeWindow) {
        DestroyWindow(mNativeWindow);
        mNativeWindow = 0;
    }

    if (mParentWindow) {
        DestroyWindow(mParentWindow);
        mParentWindow = 0;
    }

    UnregisterClassA(mParentClassName.c_str(), NULL);
    UnregisterClassA(mChildClassName.c_str(), NULL);
}


void Win32Window::messageLoop() {
    MSG msg;
    while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
}

OSWindow* CreateOSWindow() { return new Win32Window(); }

bool Win32Window::setPosition(int x, int y) {
    if (mX == x && mY == y) {
        return true;
    }

    RECT windowRect;
    if (!GetWindowRect(mParentWindow, &windowRect)) {
        return false;
    }

    if (!MoveWindow(mParentWindow, x, y, windowRect.right - windowRect.left,
                    windowRect.bottom - windowRect.top, TRUE)) {
        return false;
    }

    return true;
}

bool Win32Window::resize(int width, int height) {
    if (width == mWidth && height == mHeight) {
        return true;
    }

    RECT windowRect;
    if (!GetWindowRect(mParentWindow, &windowRect)) {
        return false;
    }

    RECT clientRect;
    if (!GetClientRect(mParentWindow, &clientRect)) {
        return false;
    }

    LONG diffX = (windowRect.right - windowRect.left) - clientRect.right;
    LONG diffY = (windowRect.bottom - windowRect.top) - clientRect.bottom;
    if (!MoveWindow(mParentWindow, windowRect.left, windowRect.top,
                    width + diffX, height + diffY, TRUE)) {
        return false;
    }

    if (!MoveWindow(mNativeWindow, 0, 0, width, height, FALSE)) {
        return false;
    }

    return true;
}

void Win32Window::setVisible(bool isVisible) {
    int flag = (isVisible ? SW_SHOW : SW_HIDE);

    ShowWindow(mParentWindow, flag);
    ShowWindow(mNativeWindow, flag);
}

void Win32Window::pushEvent(Event event) {
    OSWindow::pushEvent(event);

    switch (event.Type) {
    case Event::EVENT_RESIZED:
        MoveWindow(mNativeWindow, 0, 0, mWidth, mHeight, FALSE);
        break;
    default:
        break;
    }
}
