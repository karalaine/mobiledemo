#include "Utils.h"
#include <objbase.h>

std::string Utils::CreateGUID()
{
	GUID newId;
	CoCreateGuid(&newId);

	OLECHAR* bstrGuid;
	StringFromCLSID(newId, &bstrGuid);
	size_t origsize = wcslen(bstrGuid);
	size_t convertedChars = 0;
	std::vector<char> nstring(origsize-2);
	// Put a copy of the converted string into nstring
	wcstombs_s(&convertedChars, &nstring[0], (origsize - 2)*sizeof(char), &bstrGuid[1], _TRUNCATE);
	CoTaskMemFree(bstrGuid);
	return std::string(nstring.begin(), nstring.end());
}

std::vector<std::string> Utils::CreateGUID(int count)
{
	std::vector<std::string> guids(count);
	for (int i = 0; i < count; i++)
	{
		guids[i] = CreateGUID();
	}
	return guids;
}
