#ifndef __Log_h_
#define __Log_h_ 1

#include <iostream>
#include <windows.h>

void LOGI(const char* format);
void LOGW(const char* format);

template<typename T, typename... Targs>
void LOGI(const char* format, T value, Targs... Fargs) // recursive variadic function
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);  // Get handle to standard output
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED| FOREGROUND_GREEN | FOREGROUND_BLUE);

	for (; *format != '\0'; format++) {
		if (*format == '%') {
			std::cout << value;
			LOGI(format + 1, Fargs...); // recursive call
			return;
		}
		std::cout << *format;
	}
}

template<typename T, typename... Targs>
void LOGW(const char* format, T value, Targs... Fargs)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);  // Get handle to standard output
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN);

	for (; *format != '\0'; format++) {
		if (*format == '%') {
			std::cout << value;
			LOGW(format + 1, Fargs...); // recursive call
			return;
		}
		std::cout << *format;
	}
}

#endif
