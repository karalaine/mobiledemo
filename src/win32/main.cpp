#include "Engine.h"
#include "Log.h"
#include "Win32Window.h"

int main(int, char **)
{
	Engine engine;
	Win32Window window;
	window.initialize("ES 3 on paras ja OpenGL :D", 1280, 720);
	window.setVisible(true);
	auto nativeWindow = window.getNativeWindow();
	engine.animating = 1;

	bool running = true;
	while (running)
	{
		Event event;
		while (window.popEvent(&event))
		{
			// If the application did not catch a close event, close now
			switch (event.Type)
			{
				case Event::EVENT_CREATED:
					engine.Initialize(nativeWindow);
					break;
				case Event::EVENT_CLOSED:
					running = false;
					break;
				case Event::EVENT_MOUSE_MOVED:
					engine.Input(event.MouseMove.X, event.MouseMove.Y);
					break;
				case Event::EVENT_KEY_PRESSED:
					if (event.Key.Code == Key::KEY_ESCAPE)
					{
						running = false;
					}
					else
					{
						engine.KeyPressed(event.Key.Code);
					}
					break;
				case Event::EVENT_KEY_RELEASED:
				{
					engine.KeyReleased(event.Key.Code);
					break;
				}
				case Event::EVENT_RESIZED:
				{
					engine.Resize(event.Size.Width, event.Size.Height);
					break;
				}
			}
		
		}
		if (!running) break;
		
		engine.Update();
		window.messageLoop();
	}
	engine.DisplayTerminated();
	window.destroy();
}
