#include <assimp/mesh.h>

#include "Mesh.h"

namespace
{
	glm::vec3 to_glm(const aiVector3D& vector)
	{
		return glm::vec3(vector.x, vector.y, vector.z);
	}
}


Mesh::Mesh(std::vector<Vertex>&& vertices, std::vector<unsigned int>&& indices, const std::string & materialId)
	: m_vertices(std::move(vertices)),
	m_indices(std::move(indices)),
	m_materialGuid(materialId),
	m_triangleCount(m_indices.size()/3)
{
}

Mesh::Mesh(const aiMesh* aiMesh, const std::string& materialId) :
	m_materialGuid(materialId),
	m_triangleCount(aiMesh->mNumFaces)
{

	for (unsigned int i = 0; i < aiMesh->mNumVertices; i++)
	{
		Vertex vertex;
		
		vertex.Position = to_glm(aiMesh->mVertices[i]);

		if (aiMesh->HasNormals())
		{
			vertex.Normal = to_glm(aiMesh->mNormals[i]);
		}
		if (aiMesh->HasTextureCoords(0))
		{
			vertex.TexCoords = glm::vec2(aiMesh->mTextureCoords[0][i].x, aiMesh->mTextureCoords[0][i].y);
		}
        if (aiMesh->HasTangentsAndBitangents()) {
			vertex.Tangent = to_glm(aiMesh->mTangents[i]);
            vertex.Bitangent = to_glm(aiMesh->mBitangents[i]);
		}
		m_vertices.push_back(vertex);
	}
	for (unsigned int n = 0; n < aiMesh->mNumFaces; n++)
	{
		auto face = aiMesh->mFaces[n];
		for (unsigned int i = 0; i < face.mNumIndices; i++)
		{
			m_indices.push_back(face.mIndices[i]);
		}
	}
}

