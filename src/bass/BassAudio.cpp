#include "Audio.h"
#include "bass.h"
#include "Log.h"
#include <cassert>
#include <fstream>
#ifdef _MSC_VER
#define STDCALL __stdcall
#else
#define STDCALL __attribute__((__stdcall__))
#endif
class Audio::Impl
{
public:
	
	struct mem
	{
		char *buf = nullptr;
		QWORD length = 0;
		char * start = nullptr;
		char * end = nullptr;
	};
	// Inherited via Audio
	static void CreateEngine()
	{
		if (!BASS_Init(-1, 44100, 0, nullptr, nullptr))
		{
			std::cout << "Bass init failed: " << BASS_ErrorGetCode();
		}
	}
    static void DestroyEngine()
	{
		BASS_Free();
	}

	bool Open(const std::string & fileName, AssetManager & mgr) {
		using std::ios_base;
		auto& file = mgr.Open(fileName, ios_base::in | ios_base::binary);
	
		auto closeCb = [](void*) 
		{
			std::cout << "Close requested, handled by asset system";
		};
		auto fileLen = [](void* user)
		{
			auto* src = static_cast<Asset*>(user);
			return static_cast<QWORD>(src->GetLength());
        };

		auto fileRead = [] (void *buffer, DWORD length, void *user)
		{
            auto* src = static_cast<Asset*>(user);
			auto rd = src->Read(static_cast<char*>(buffer), length);
			return static_cast<DWORD>(rd); // read from file
		};

		auto fileSeek = [](QWORD offset, void *user)
		{
            auto* src = static_cast<Asset*>(user);
			bool isAvailable = src->Seek(static_cast<size_t>(offset)) != -1;

			return static_cast<BOOL>(isAvailable); // seek to offset
		};

		BASS_FILEPROCS procs = { closeCb, fileLen, fileRead, fileSeek };
			
		if (0 == (m_channel = BASS_StreamCreateFileUser(STREAMFILE_NOBUFFER, BASS_SAMPLE_FLOAT, &procs, &file))) // create the stream
		{
			BassError();
		}
		return m_channel != 0;
	}
	void Pause() {
		if (m_channel)
			if (!BASS_ChannelPause(m_channel))
			{
				BassError();
			}
	}
	void Play()  {
		if (m_channel)
			if (!BASS_ChannelPlay(m_channel, false))
			{
				BassError();
			}
	}

	int GetPos()
	{
		if (m_channel)
		{
			QWORD pos = BASS_ChannelGetPosition(m_channel, BASS_POS_BYTE);
			return static_cast<int>(1000 * BASS_ChannelBytes2Seconds(m_channel, pos));
		}
		return -1;
	}

	void SetPos(int pos)
	{
		QWORD bytePos = BASS_ChannelSeconds2Bytes(m_channel, pos / 1000.0);
		BASS_ChannelSetPosition(m_channel, bytePos, BASS_POS_BYTE);
	}

	bool IsPlaying()
	{
		return BASS_ChannelIsActive(m_channel) == BASS_ACTIVE_PLAYING;
	}
private:
	DWORD m_channel;
	void BassError()
	{
		int error = BASS_ErrorGetCode();
		std::cout << "Bass loading sound failed: " << error;
	}
};

Audio::Audio() :
m_impl(new Impl())
{

}
Audio::~Audio()
{
	m_impl->DestroyEngine();
}
void Audio::CreateEngine()
{
	m_impl->CreateEngine();
}

bool Audio::Open(const std::string & fileName, AssetManager & mgr) 
{
	return m_impl->Open(fileName, mgr);
}

void Audio::Pause()
{
	m_impl->Pause();
}

void Audio::Play()
{
	m_impl->Play();
}

int Audio::GetPositionInMillis()
{
	return m_impl->GetPos();
}

void Audio::SetPositionInMillis(int pos)
{
	m_impl->SetPos(pos);
}

bool Audio::IsPlaying()
{
	return m_impl->IsPlaying();
}
