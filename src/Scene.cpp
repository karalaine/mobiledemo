#include <assimp/scene.h>
#ifdef USE_GLAD
#include <glad/glad.h>
#else
#include <GLES3/gl3.h>
#endif
#include "Material.h"
#include "Mesh.h"
#include "MeshNode.h"
#include "Node.h"
#include "Resources.h"
#include "Scene.h"
#include "Texture.h"
#include "Utils.h"

namespace {
enum DepthMapType { DIR = 0, POINTL = 1 };
}



Scene::Scene(bool useDepthmap)
    : m_depthMapFBO{}, m_depthMapTexture{} {
    if (useDepthmap) {
        GenerateDepthFBO();
	}
    /*
            glBindTexture(GL_TEXTURE_CUBE_MAP,
       m_depthMapTexture[DepthMapType::POINTL]);
            const GLuint SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

            for (GLuint i = 0; i < 6; ++i)
                    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
       GL_DEPTH_COMPONENT,
                            SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT,
       GL_FLOAT, NULL);

            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER,
       GL_NEAREST);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER,
       GL_NEAREST);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S,
       GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T,
       GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R,
       GL_CLAMP_TO_EDGE);


            glBindFramebuffer(GL_FRAMEBUFFER,
       m_depthMapFBO[DepthMapType::POINTL]);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
       m_depthMapTexture[DepthMapType::POINTL], 0);
            glDrawBuffers(1, GL_NONE);
            glReadBuffer(GL_NONE);*/
}


void Scene::GenerateDepthFBO() {

    glGenFramebuffers(2, m_depthMapFBO.data());
    // - Create depth textures
    glGenTextures(2, m_depthMapTexture.data());
    glBindTexture(GL_TEXTURE_2D, m_depthMapTexture[DepthMapType::DIR]);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, 4096, 4096, 0,
                 GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    GLfloat borderColor[] = {1.0, 1.0, 1.0, 1.0};
#ifdef GL_TEXTURE_BORDER_COLOR
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
#endif
    glBindFramebuffer(GL_FRAMEBUFFER, m_depthMapFBO[DIR]);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
                           m_depthMapTexture[DIR], 0);
    // glDrawBuffers(1, GL_NONE);
    glReadBuffer(GL_NONE);
}

void Scene::InitializeScene() {
    for (auto node : m_nodes) {
        node->InitNode();
    }
}

void Scene::Render() {
    std::vector<Node * > transparentNodes {};
    for (auto node : m_nodes) {
        node->RenderOpaque();
        node->CollectTransparent(transparentNodes);
    }
    for (auto node : transparentNodes) {
        node->RenderTransparent();
    }
}

void Scene::RenderDepthMap() {
    if (m_depthMapFBO[DIR] == 0)
        return;
    glViewport(0, 0, 4096, 4096);
    glBindFramebuffer(GL_FRAMEBUFFER, m_depthMapFBO[DIR]);
    glClear(GL_DEPTH_BUFFER_BIT);
    auto depthMapShader = Resources::Get().Shaders().Get("depthMap");
    glUseProgram(*depthMapShader);
    for (auto& light : m_lights) {
        if (light.GetType() == Light::Type::Directional) {
            auto lightMatrix = light.GetSpaceMatrix();
            GLint uniProjection = glGetUniformLocation(*depthMapShader, "lightSpaceMatrix");
            if (uniProjection != -1)
                glUniformMatrix4fv(uniProjection, 1, GL_FALSE, glm::value_ptr(lightMatrix));

            for (auto node : m_nodes) {
                node->RenderDepth(*depthMapShader);
            }
        } else if (light.GetType() == Light::Type::Point) {
        }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_DEPTH_BUFFER_BIT);
}

GLuint Scene::GetDepthMap() const { return m_depthMapTexture[DIR]; }

void Scene::ClearNodes() { m_nodes.clear(); }

void Scene::AddNode(std::shared_ptr<Node> node) { m_nodes.push_back(node); }

void Scene::AddLight(Light&& light) { m_lights.emplace_back(light); }

std::vector<Light>& Scene::GetLights() { return m_lights; }
