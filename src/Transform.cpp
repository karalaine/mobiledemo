#include "Transform.h"
#define GLM_FORCE_RADIANS 1
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

Transform::Transform(Transform* parent)
    : m_parent(parent), 
	m_localMatrix(1),
	m_position(0),
	m_rotation(),
	m_scale(1),
	m_transformDirty(true)
{
}

Transform::Transform(glm::mat4 transform, Transform* parent)
	: m_parent(parent),
	m_localMatrix(transform),
	m_position(0),
	m_rotation(),
	m_scale(1), 
	m_transformDirty(true) 
{
}

void Transform::SetPosition(const glm::vec3 & pos)
{
	m_transformDirty = true;
	m_position = pos;
}

glm::vec3 Transform::GetPosition() const
{
	return m_position;
}

void Transform::SetRotation(const glm::vec3 & rot)
{
	m_transformDirty = true;
	m_rotation = glm::quat(rot);
	
}

glm::vec3 Transform::GetRotation() const
{
	return glm::eulerAngles(m_rotation);
}

void Transform::SetScale(const glm::vec3 & scale)
{
	m_transformDirty = true;
	m_scale = scale;
}

glm::vec3 Transform::GetScale()
{
	return m_scale;
}

void Transform::Translate(const glm::vec3& translation, bool local)
{
	m_transformDirty = true;
	m_position += translation;
}

void Transform::Scale(const glm::vec3& scale)
{
	m_transformDirty = true;
	m_scale *= scale;
}

void Transform::SetParent(Transform * parent)
{
	m_parent = parent;
}


void Transform::Rotate(const glm::vec3& rotation, float amount)
{
	m_transformDirty = true;
	m_rotation = glm::rotate(m_rotation, amount, rotation);
}

glm::mat4 Transform::GetWorldMatrix()
{
	if (m_transformDirty)
	{
		auto scaleMatrix = glm::scale(glm::mat4(1), m_scale);
		auto rotationMatrix = glm::mat4_cast(m_rotation);
		m_localMatrix = glm::translate(rotationMatrix*scaleMatrix, m_position);
		m_transformDirty = false;
	}
	if(m_parent != nullptr)
		return m_parent->GetWorldMatrix() * m_localMatrix;

	return m_localMatrix;
}


