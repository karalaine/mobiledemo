#pragma once
#ifdef USE_GLAD
#include <glad/glad.h>
#else
#include <GLES3/gl3.h>
#endif
#include "Shader.h"
#include "Resources.h"

//TEMP MOCKUP
class RenderPass
{
  public:
	RenderPass(GLuint colorBuffer, const std::string& shader) : textureColorbuffer(colorBuffer)
	{
		Shader screenShader(&Resources::Get().Assets(), shader);
		m_screenShader = screenShader;
	}
	GLint m_screenShader;
	GLuint textureColorbuffer;
	void Render()
	{
		glUseProgram(m_screenShader);
		glDisable(GL_DEPTH_TEST);
		glBindTexture(GL_TEXTURE_2D, textureColorbuffer);
		glDrawArrays( GL_TRIANGLES, 0, 3);
		glEnable(GL_DEPTH_TEST);
	}
};