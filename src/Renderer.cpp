#include "Renderer.h"
#include "RenderPass.h"
#include "Scene.h"

#include <algorithm>
#ifdef _MSC_VER
#define STDCALL __stdcall
#else
#define STDCALL __attribute__((__stdcall__))
#endif
#if USE_GLAD
#include <glad/glad.h>
#define GLAD_VERSION_MAJOR(version) (version / 10000)
#define GLAD_VERSION_MINOR(version) (version % 10000)
#else
#define GL_GLEXT_PROTOTYPES
#include <GLES3/gl3.h>
#include <GLES3/gl32.h>
#endif //USE_GLAD
#ifdef USE_WGL
#include <glad/glad_wgl.h>
#else
#include <EGL/egl.h>
#include <EGL/eglext.h>
#endif //USE_WGL
#include "Log.h"

Renderer::Renderer() : m_context(), m_display(), m_height(), m_surface(), m_width() {}

Renderer::~Renderer() {}

static void printGLString(const char* name, GLenum s) {
    const char * result = reinterpret_cast<const char *>(glGetString(s));
    if(result == nullptr) {
        LOGI("GL % = nullptr\n", name);
        return;
    }
    LOGI("GL % = %\n", name, result);
}

#ifdef USE_WGL
namespace {
HDC deviceContext;
}

int Renderer::InitDisplay(EGLNativeWindowType window) {
    PIXELFORMATDESCRIPTOR pfd = {
        sizeof(PIXELFORMATDESCRIPTOR),
        1,
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER, // Flags
        PFD_TYPE_RGBA, // The kind of framebuffer. RGBA or palette.
        32,            // Colordepth of the framebuffer.
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        24, // Number of bits for the depthbuffer
        8,  // Number of bits for the stencilbuffer
        0,  // Number of Aux buffers in the framebuffer.
        PFD_MAIN_PLANE,
        0,
        0,
        0,
        0};

    deviceContext = GetDC(window);
 
    int pixlFormat;
    pixlFormat = ChoosePixelFormat(deviceContext, &pfd);
    SetPixelFormat(deviceContext, pixlFormat, &pfd);
    auto windowContext = wglCreateContext(deviceContext);
    wglMakeCurrent(deviceContext, windowContext);

    gladLoadWGL(deviceContext);

    const int attribList[] = {
        WGL_DRAW_TO_WINDOW_ARB,
        GL_TRUE,
        WGL_SUPPORT_OPENGL_ARB,
        GL_TRUE,
        WGL_DOUBLE_BUFFER_ARB,
        GL_TRUE,
        WGL_PIXEL_TYPE_ARB,
        WGL_TYPE_RGBA_ARB,
        WGL_COLOR_BITS_ARB,
        32,
        WGL_DEPTH_BITS_ARB,
        24,
        WGL_STENCIL_BITS_ARB,
        8,
        WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB,
        GL_TRUE,
        WGL_SAMPLE_BUFFERS_ARB,
        GL_TRUE,
        WGL_SAMPLES_ARB,
        4,
        0, // End
    };
    UINT numFormats;
    wglChoosePixelFormatARB(deviceContext, attribList, nullptr, 1, &pixlFormat,
                            &numFormats);

    int attribs[] = {WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,
                     WGL_CONTEXT_PROFILE_MASK_ARB,
                     WGL_CONTEXT_CORE_PROFILE_BIT_ARB, 0};
    auto context = wglCreateContextAttribsARB(deviceContext, 0, attribs);
    wglMakeCurrent(deviceContext, context);
    wglSwapIntervalEXT(1);
    wglDeleteContext(windowContext);
#if USE_GLAD
    gladLoadGL();
#endif
    GLint majorVersion = 1;
    GLint minorVersion = 0;
    // Check what we did get
    glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
    glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
    std::cout << "Context created with: " << majorVersion << "." << minorVersion
              << std::endl;
    RECT windowSize;
    GetWindowRect(window, &windowSize);
    m_width = windowSize.right - windowSize.left;
    m_height = windowSize.bottom - windowSize.top;
    glViewport(0, 0, m_width, m_height);
    glEnable(GL_DEPTH_TEST);
    // glDepthMask(GL_FALSE);
    glDepthFunc(GL_LESS);
    glEnable(GL_FRAMEBUFFER_SRGB);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_BLEND);
    glEnable(GL_MULTISAMPLE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE,
                       // GL_ONE_MINUS_SRC_ALPHA);
    printGLString("Version", GL_VERSION);
    printGLString("Vendor", GL_VENDOR);
    printGLString("Renderer", GL_RENDERER);
    printGLString("Extensions", GL_EXTENSIONS);
    printGLString("GLSL version", GL_SHADING_LANGUAGE_VERSION);

    GLfloat fLargest;
#ifdef GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
#elif GL_MAX_TEXTURE_MAX_ANISOTROPY 
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY, &fLargest);
#endif
    // Clear color with black color
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    std::cout << std::endl;
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(
        [](GLenum, GLenum, GLuint, GLenum severity, GLsizei,
           const GLchar* message, const void*) {
            if (severity == GL_DEBUG_SEVERITY_HIGH) {
                LOGW("%s", message);
            } else {
                LOGI("%s", message);
            }
        },
        nullptr);
    m_display = deviceContext;
    return 0;
}

void Renderer::TermDisplay() { wglDeleteContext(0); }
#else

int Renderer::InitDisplay(EGLNativeWindowType window) {
    // initialize OpenGL ES and EGL

    /*
     * Here specify the attributes of the desired configuration.
     * Below, we select an EGLConfig with at least 8 bits per color
     * component compatible with on-screen windows
     */
    const double glVersion = 3.0;
    const EGLint attribs[] = {

        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_DEPTH_SIZE,   8,
        EGL_BLUE_SIZE,    8,
        EGL_GREEN_SIZE,   8,
        EGL_RED_SIZE,     8,
        EGL_NONE};
    EGLint format;
    EGLint numConfigs;
    EGLConfig config;
    EGLSurface surface;
    EGLContext context;

    /*EGLint angle_attribs[] = {
            EGL_PLATFORM_ANGLE_TYPE_ANGLE,
            EGL_PLATFORM_ANGLE_TYPE_OPENGLES_ANGLE,
            EGL_NONE
    }; */
    // auto display = eglGetPlatformDisplayEXT(EGL_PLATFORM_ANGLE_ANGLE,
    // EGL_DEFAULT_DISPLAY, angle_attribs);

    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

    // audio.CreateBufferQueueAudioPlayer();

    eglInitialize(display, 0, 0);

    /* Here, the application chooses the configuration it desires. In this
     * sample, we have a very simplified selection process, where we pick
     * the first EGLConfig that matches our criteria */
    eglChooseConfig(display, attribs, &config, 1, &numConfigs);

    /* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
     * guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
     * As soon as we picked a EGLConfig, we can safely reconfigure the
     * ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */
    eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);

    // ANativeWindow_setBuffersGeometry(window, 0, 0, format);

    surface = eglCreateWindowSurface(display, config, window, NULL);

    const EGLint contextAttr[] = {
        EGL_CONTEXT_CLIENT_VERSION, static_cast<EGLint>(glVersion),
        EGL_CONTEXT_FLAGS_KHR, EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR, EGL_NONE};

    context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttr);
    if (context == EGL_NO_CONTEXT) {
            LOGW("Unable to create EGL context (eglError: %)\n",
                   eglGetError());
            return 1;
    }
    
    if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE) {
        LOGW("Unable to eglMakeCurrent");
        return -1;
    }

    eglQuerySurface(display, surface, EGL_WIDTH, &m_width);
    eglQuerySurface(display, surface, EGL_HEIGHT, &m_height);

    m_display = display;
    m_context = context;
    m_surface = surface;
#if USE_GLAD
    int loadedVersion = gladLoadGL();
    if(loadedVersion != 0) {
        LOGI("Glad loaded GL: %.%\n", GLVersion.major, GLVersion.minor);
    }
#endif
    
    glViewport(0, 0, m_width, m_height);
    // Initialize GL state.
    glEnable(GL_DEPTH_TEST);
    // glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    auto debugCallback = [](GLenum, GLenum, GLuint, GLenum severity, GLsizei,
                            const GLchar* message, const void*) {
                             if (severity == GL_DEBUG_SEVERITY_HIGH)
                                 std::cerr << message << std::endl;
                             else
                                 std::cerr << message << std::endl;
                         };
    if (glDebugMessageCallback != nullptr) {
        glDebugMessageCallback(debugCallback, nullptr);
    } else if(glDebugMessageCallbackKHR != nullptr) {
        glDebugMessageCallbackKHR(debugCallback, nullptr);
    }
    // Print the OpenGL ES system metrics
    printGLString("Version", GL_VERSION);
    printGLString("Vendor", GL_VENDOR);
    printGLString("Renderer", GL_RENDERER);
    printGLString("Extensions", GL_EXTENSIONS);
    printGLString("GLSL version", GL_SHADING_LANGUAGE_VERSION);
    // glEnable(0x8DB9); //GL_FRAMEBUFFER_SRGB
    return 0;
}

void Renderer::TermDisplay() {

    if (m_display != EGL_NO_DISPLAY) {
        eglMakeCurrent(m_display, EGL_NO_SURFACE, EGL_NO_SURFACE,
                       EGL_NO_CONTEXT);
        if (m_context != EGL_NO_CONTEXT) {
            eglDestroyContext(m_display, m_context);
        }
        if (m_surface != EGL_NO_SURFACE) {
            eglDestroySurface(m_display, m_surface);
        }
        eglTerminate(m_display);
    }
    m_display = EGL_NO_DISPLAY;
    m_context = EGL_NO_CONTEXT;
    m_surface = EGL_NO_SURFACE;
}

#endif
void Renderer::InitializeScenes() {
    for (auto& scene : m_scenes)
        scene->InitializeScene();
}

void Renderer::Resize(int w, int h) {
    m_width = w;
    m_height = h;
    glViewport(0, 0, w, h);
}

void Renderer::Render() {
    if (m_display == nullptr) {
        // No display.
        return;
    }
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (const auto& scene : m_scenes) {
        scene->RenderDepthMap();

        // Reset viewport
        glViewport(0, 0, m_width, m_height);
        scene->Render();
    }
    for (auto& renderPass : m_renderPasses) {
        renderPass->Render();
    }
#ifdef USE_WGL
    SwapBuffers(deviceContext);
#else
    eglSwapBuffers(m_display, m_surface);
#endif
}

void Renderer::AddScene(std::unique_ptr<Scene>&& scene) {
    m_scenes.push_back(std::move(scene));
}

bool Renderer::RemoveScene(Scene* scene) {
    return m_scenes.erase(std::remove_if(m_scenes.begin(), m_scenes.end(),
                                         [=](std::unique_ptr<Scene>& cmp) {
                                             return scene == cmp.get();
                                         }),
                          m_scenes.end()) != m_scenes.end();
}

void Renderer::AddRenderPass(std::unique_ptr<RenderPass>&& renderPass) {
    m_renderPasses.push_back(std::move(renderPass));
}

EGLint Renderer::GetWidth() { return m_width; }

EGLint Renderer::GetHeight() { return m_height; }
