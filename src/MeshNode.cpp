#include "MeshNode.h"
#include "Resources.h"
#include <assimp/scene.h>
#include <glm/gtc/type_ptr.hpp>

namespace {
glm::mat4 to_glm(const aiMatrix4x4& matrix) {
    return glm::transpose(glm::mat4(
        matrix.a1, matrix.a2, matrix.a3, matrix.a4, matrix.b1, matrix.b2,
        matrix.b3, matrix.b4, matrix.c1, matrix.c2, matrix.c3, matrix.c4,
        matrix.d1, matrix.d2, matrix.d3, matrix.d4));
}
} // namespace

MeshNode::MeshNode(std::vector<std::string>& meshGuids)
    : m_meshIds(meshGuids), m_hasTangents(false) {}

MeshNode::MeshNode(aiNode* node, std::vector<std::string>& meshGuids)
    : Node(to_glm(node->mTransformation)), m_meshIds(node->mNumMeshes),
      m_hasTangents(true) {
    for (int i = 0; i < node->mNumMeshes; i++) {
        auto meshIndex = node->mMeshes[i];
        m_meshIds[i] = meshGuids[meshIndex];
    }

    for (unsigned int n = 0; n < node->mNumChildren; n++) {
        AddChild(new MeshNode(node->mChildren[n], meshGuids),
                 node->mChildren[n]->mName.C_Str());
    }
}

void MeshNode::InitNode() {
    if (m_meshIds.size() > 0) {
        // Query and send the uniform variable.

        m_vaos.resize(1);
        glGenVertexArrays(1, &m_vaos[0]);
        std::vector<GLuint> vbo(m_meshIds.size());
        glGenBuffers(m_meshIds.size(), &vbo[0]);
        std::vector<GLuint> EBO(m_meshIds.size());

        glGenBuffers(m_meshIds.size(), &EBO[0]);

        glBindVertexArray(m_vaos[0]);
        int meshIndex = 0;
        for (auto meshId : m_meshIds) {
            auto mesh = Resources::Get().Meshes().Get(meshId);
            if (mesh) {
                auto material =
                    Resources::Get().Materials().Get(mesh->GetMaterialGuid());
                material->LoadAllTextures();

                GLuint programId = material->GetShaderProgram();
                glBindBuffer(GL_ARRAY_BUFFER, vbo[meshIndex]);
                glBufferData(GL_ARRAY_BUFFER,
                             sizeof(Vertex) * mesh->GetVertexCount(),
                             mesh->GetVertexData(), GL_STATIC_DRAW);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO[meshIndex]);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                             sizeof(GLuint) * mesh->GetIndexCount(),
                             mesh->GetIndexData(), GL_STATIC_DRAW);
                glUseProgram(programId);
                auto normalOffset = (GLvoid*)offsetof(Vertex, Normal);
                auto texCoordOffet = (GLvoid*)offsetof(Vertex, TexCoords);
                glEnableVertexAttribArray(0);
                glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                                      (GLvoid*)0);
                glEnableVertexAttribArray(1);
                glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                                      normalOffset);
                glEnableVertexAttribArray(2);
                glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                                      texCoordOffet);
                if (m_hasTangents) {
                    auto tangetOffset = (GLvoid*)offsetof(Vertex, Tangent);
                    auto bitangetOffet = (GLvoid*)offsetof(Vertex, Bitangent);
                    glEnableVertexAttribArray(3);
                    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE,
                                          sizeof(Vertex), tangetOffset);
                    glEnableVertexAttribArray(4);
                    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE,
                                          sizeof(Vertex), bitangetOffet);
                }
                glUseProgram(0);
                glBindVertexArray(0);
            }
            meshIndex++;
        }

        // Bind back to the default state.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    for (auto node : m_children) {
        node.second->InitNode();
    }
}

void MeshNode::CalcTangents() {
    for (auto& id : m_meshIds) {
        auto mesh = Resources::Get().Meshes().Get(id);

        for (unsigned int i = 0; i < mesh->GetTriangleCount(); i++) {
            auto& data = mesh->GetVertices();
            auto& indexes = mesh->GetIndices();
            auto& vertex1 = data[indexes[0 + i * 3]];
            auto& vertex2 = data[indexes[1 + i * 3]];
            auto& vertex3 = data[indexes[2 + i * 3]];
            glm::vec3 pos1 = vertex1.Position;
            glm::vec3 pos2 = vertex2.Position;
            glm::vec3 pos3 = vertex3.Position;
            // texture coordinates
            glm::vec2 uv1 = vertex1.TexCoords;
            glm::vec2 uv2 = vertex2.TexCoords;
            glm::vec2 uv3 = vertex3.TexCoords;

            glm::vec3 edge1 = pos2 - pos1;
            glm::vec3 edge2 = pos3 - pos1;
            glm::vec2 deltaUV1 = uv2 - uv1;
            glm::vec2 deltaUV2 = uv3 - uv1;

            GLfloat f =
                1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
            glm::vec3 tangent, bitangent;
            tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
            tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
            tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
            tangent = glm::normalize(tangent);
            vertex1.Tangent = tangent;
            vertex2.Tangent = tangent;
            vertex3.Tangent = tangent;

            bitangent.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
            bitangent.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
            bitangent.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
            bitangent = glm::normalize(bitangent);

            vertex1.Bitangent = bitangent;
            vertex2.Bitangent = bitangent;
            vertex3.Bitangent = bitangent;
        }
    }
    m_hasTangents = true;
}

void MeshNode::Render(const bool transparentPass) {
    if (m_meshIds.size() > 0) {
        auto& res = Resources::Get();
        for (auto meshId : m_meshIds) {
            auto mesh = res.Meshes().Get(meshId);
            auto material = res.Materials().Get(mesh->GetMaterialGuid());
            auto alphaTex = res.Textures().Get(material->GetTexture(TextureType::AlphaMask));
            if (!transparentPass && (alphaTex != nullptr)) {
                continue;
            }
            GLuint programId = material->GetShaderProgram();
            auto diffuseTexture =
                res.Textures().Get(material->GetTexture(TextureType::Diffuse));
            glUseProgram(programId);
            GLint uniModel = glGetUniformLocation(programId, "model");
            GLint uniNormal = glGetUniformLocation(programId, "normalMatrix");
            auto modelMatrix = m_transform.GetWorldMatrix();
            glUniformMatrix4fv(uniModel, 1, GL_FALSE,
                               glm::value_ptr(modelMatrix));
            auto normalMatrix =
                glm::mat3(glm::transpose(glm::inverse(modelMatrix)));
            glUniformMatrix3fv(uniNormal, 1, GL_FALSE,
                               glm::value_ptr(normalMatrix));
            glBindVertexArray(m_vaos[0]);
            glActiveTexture(GL_TEXTURE0);
            if (diffuseTexture) {
                glBindTexture(diffuseTexture->GetBindTarget(),
                              diffuseTexture->GetId());
            }
           
            if (alphaTex) {
                glActiveTexture(GL_TEXTURE2);
                glBindTexture(alphaTex->GetBindTarget(), alphaTex->GetId());
                glActiveTexture(GL_TEXTURE0);
            }
            auto normalTex = res.Textures().Get(
                material->GetTexture(TextureType::Normal));
            if (normalTex) {
                glActiveTexture(GL_TEXTURE3);
                glBindTexture(normalTex->GetBindTarget(), normalTex->GetId());
                glActiveTexture(GL_TEXTURE0);
            }
            glDrawElements(GL_TRIANGLES, mesh->GetIndexCount(), GL_UNSIGNED_INT,
                           0);
            glUseProgram(0);
        }
        glBindVertexArray(0);
    }
    
}
void MeshNode::RenderOpaque() {
    Render(false);
    for (auto node : m_children) {
        node.second->RenderOpaque();
    }
}

void MeshNode::RenderTransparent() {
    Render(true);
}
void MeshNode::CollectTransparent(std::vector<Node *>& transparentList){
    auto& res = Resources::Get();
    for (auto meshId : m_meshIds) {
        auto mesh = res.Meshes().Get(meshId);
        auto material = res.Materials().Get(mesh->GetMaterialGuid());
        auto hasAlpha = nullptr != res.Textures().Get(material->GetTexture(TextureType::AlphaMask));
        if(hasAlpha) {
            transparentList.push_back(this);
        }        
    }
    for (auto node : m_children) {
        node.second->CollectTransparent(transparentList);
    }
}
void MeshNode::RenderDepth(Shader& depthShader) {
    if (m_meshIds.size() > 0) {
        for (auto meshId : m_meshIds) {
            auto mesh = Resources::Get().Meshes().Get(meshId);
            glUseProgram(depthShader);
            GLint uniModel = glGetUniformLocation(depthShader, "model");
            auto modelMatrix = m_transform.GetWorldMatrix();
            glUniformMatrix4fv(uniModel, 1, GL_FALSE,
                               glm::value_ptr(modelMatrix));
            glBindVertexArray(m_vaos[0]);
            glDrawElements(GL_TRIANGLES, mesh->GetIndexCount(), GL_UNSIGNED_INT,
                           0);
            glUseProgram(0);
        }
        glBindVertexArray(0);
    }
    for (auto node : m_children) {
        node.second->RenderDepth(depthShader);
    }
}
